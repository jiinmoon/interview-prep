""" 264. Ugly Number II

Question:

    Write a program to find the n-th ugly number.

    Ugly numbers are positive numbers whose prime factors include 2, 3, and 5.

+++

Attempt:

    Obvious solution is to generate nums and to check its prime compositions.

    Or, we work backwards - starting from 1, what are the primes that can be
    made from using only the factors 2, 3, and 5?

    [ '1' ] -> (1 * 2, 1 * 3, 1 * 5) -> [ 2, 3, 5 ]
    [ 1, '2', 3, 5 ] -> (2 * 2, 2 * 3, 2 * 5) -> [ 4, 6, 10 ]
    [ 1, 2, '3', 4, 5, 6, 10 ] -> (3 * 2, ...)

    The problem is that the numbers get disorganized when we generate and then
    try to put it back into the original array. There are few data structures
    that can help with this - priority queue or minHeap.

+++

Result:

    Submission accepted at 1284 ms. Also, there exists a DP solution, which is a
    lot better with 140 ms. The idea behind it is intriguing and should study
    further on it.

"""

# heapq is heap priority queue lib.
from heapq import heappush, heappop

class Solution:
    def __init__(self):
        self.table = []
        self.UPPER_BOUND = 1690
        # we know the upper bound of n <= 1690.
        # generate them such that subsequent calls would be faster.
        heap = [1]
        for i in range(self.UPPER_BOUND):
            currMin = heapop(heap)
            self.table.append(currMin)
            # discard duplicates
            while len(heap) != 0 and heap[0] == currMin:
                currMin = heappop(heap)
            # generate next vals
            for nextVal in [currMin*2, currMin*3, currMin*5]:
                heappush(heap, nextVal)
        self.table.append(heap[0])

    def nthUglyNumber(self, n):
        return self.table[n-1]

# ---

class Solution2:
    def nthUglyNumber(self, n):
        if n <= 1: return n
        # pointers for primes 2, 3, and 5.
        p2, p3, p5 = 0, 0, 0
        result = [1] * n
        for i in range(1, n):
            v2, v3, v5 = result[p2] * 2, result[p3] * 3, result[p5] * 5
            result[i] = min(v2, v3, v5)
            if result[i] == v2:
                p2 += 1
            if result[i] == v3:
                p3 += 1
            if result[i] == v5:
                p5 += 1
        return result[n-1]
