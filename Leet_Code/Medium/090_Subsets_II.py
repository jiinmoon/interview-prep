""" 90. Subsets II

Question:

    Given a collection of integers that might contain duplicates, nums, return
    all possible subsets.

+++

Attempt:

    This question is slightly hard to comprehend - but all its asking is that
    you do not add duplicate subsets that results from having duplicate values
    with the given array. Thus, we require a way to skip the same elements -
    sort the array first to detect the duplicate easier.

    Previously, we have added the num to every set within the result list.
    However, if the current num is duplicate of previous iteration, we only want
    to work with subsets that which have been added in previous iteration only.

    [ 1, 1, 2, 2, 3 ]

    [ [] ]
    [ [], [1] ]
    [ [], [1], [1, 1] ]
    [ [], [1], [1, 1], [2], [1, 2], [1, 1, 2] ]
    [ [], [1], [1, 1], [2], [2, 2], [1, 2], [1, 2, 2], [1, 1, 2], [1, 1, 2, 2] ]
+++

Result:

    Submission accepted at 36 ms. Tried it without backtrack at first, but did
    not work out too well.

"""

class Solution:
    def powerSetWithDups(self, nums):
        # TODO: Work in progress
        nums.sort()
        prevIdx = 0
        result = [[]]
        for i in range(len(nums)):
            currIdx = 0
            if i > 0 and nums[i] == nums[i-1]:
                currIdx = prevIdx + 1
            prevIdx = len(result)
            for currSet in result[currIdx:prevIdx]:
                temp = currSet.copy()
                temp.append(nums[i])
                result.append(temp)
        return result

class Solution2:
    def subsetsWithDup(self, nums):
        result = []
        nums.sort()

        def backtrack(nums, index, path):
            result.append(list(path))
            for i in range(index, len(nums)):
                if i > index and nums[i-1] == nums[i]:
                    continue
                backtrack(nums, i+1, path + [nums[i]])

        backtrack(nums, 0, [])
        return result
