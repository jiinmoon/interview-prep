""" 16. 3Sum Closest

Question:

    Similar to the previous 3Sum question, but now, we are to find three
    integers such that adds closest to the given target value.

+++

Attempt:

    We can use the same approach - but maintain a closestThusFar variable to
    keep track of the three integers that has been closest to the target value.

+++

Result:

    Submission accepted at 344 ms. We could have use dictionary to store
    {diff:currSum}.

"""

class Solution:

    def find3SumClosest(self, nums, target):
        if len(nums) <= 3: return sum(nums)
        nums.sort()
        result = []
        closestThusFar = float('inf')
        for i in range(len(nums)-2):
            j = i + 1
            k = len(nums)-1
            while j < k:
                a, b, c = nums[i], nums[j], nums[k]
                currSum = a + b + c
                diff = abs(target - currSum)
                if diff < closestThusFar:
                    result = [a, b, c]
                    closestThusFar = diff
                if currSum < target:
                    j += 1
                else:
                    k -= 1
        return sum(result)

class Solution2:

    def find3SumClosest(self, nums, target):
        if len(nums) <= 3: return sum(nums)
        nums.sort()
        diffMap = {}
        for i in range(len(nums)-2):
            if i > 0 and nums[i-1] == nums[i]:
                continue
            j = i + 1
            k = len(nums)-1

            while j < k:
                currSum = nums[i] + nums[j] + nums[k]
                diff = abs(target - currSum)
                diffMap[diff] = currSum
                if currSum < target:
                    j += 1
                else:
                    k -= 1
        return diffMap[min(diffMap)]
