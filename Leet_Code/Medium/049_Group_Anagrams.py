""" 49. Group Anagrams

Question:

    Given an array of strings, group anagrams together.

+++

Attempt:

    This can be very simply approached by preparing a appropriate data
    structure. In this case, we can prepare a hashmap where key is the sorted
    word and its value is the list of words.

+++

Result:

    Submission accepted at 104 ms. We could try to think of some other way to
    create the key other than sorting the words each time. Perhaps, ASCII
    encoding numbers may do it?

"""

from collections import defaultdict

class Solution:
    def groupAnagrams(self, strs):
        result = defaultdict(list)
        for word in strs:
            result[tuple(sorted(word))].append(word)
        return result.values()
