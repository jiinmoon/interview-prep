""" 146 LRU Cache

Question:

    Design and implement a data structure for Least Recently Used cache. It
    should support the following operations: get and put.

    get(key) - get the value of the key if exists; otherwise return -1.

    put(key, value) - set or insert value if key is not already present. when
    the cache reached its capacity, it should invalidate the least recently used
    item before inserting a new item.

+++

Attempt:

    At first glance, this may appear to be a hashmap problem with key:value
    needs to be supported. And to be honest, there is alredy ordereddict() that
    will make this question rather trivial to solve.

    We need a hashmap for quickly looking up the key and return the values. And
    underneath, we need another structure that can maintain a capacity while
    support get and put operations in a O(1) time. A doubly linked list allows
    for that.

+++

Result:

    Submission accepted at 484 ms. Of course, Python list does not support the
    remove at constant time - probably is the reason behind relatively high
    runtime. Too much work to implement doubly linked list.

"""

class Unit:
    def __init__(self, key, val):
        self.key = key
        self.val = val

class Solution:
    def __init__(self, capacity):
        self.capacity = capacity
        self.keyMap = dict()
        self.lruCache = []

    def get(self, key):
        if key not in self.keyMap: return -1
        self.lruCache.remove(self.keyMap[key])
        self.lruCache.append(self.keyMap[key])
        return self.keyMap[key]

    def set(self, key, val):
        if key in self.keyMap:
            self.keyMap[key].val = val
            self.lruCache.remove(self.keyMap[key])
            self.lruCache.append(self.keyMap[key])
            return

        if len(self.keyMap) >= self.capacity:
            del self.keyMap[self.lruCache.pop(0).val]

        self.keyMap[key] = Unit(key, val)
        self.lruCache.append(self.keyMap[key])

