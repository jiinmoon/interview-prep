""" 75. Sort Colors

Question:

    Given an array with n objects colored R, W, or B, sort them in-place such
    that they appear in the order. We will use integer to represent colors 0, 1,
    and 2.

+++

Attempt:

    The challenge of the question is thinking of a way to sort this in a single
    pass with only constant space - thus, sorting the nums in-place. To achieve
    this, we can use two pointer method. Let us have pointers that marks the
    swap positions for two values. As we iterate the array, if we see the value
    that needs to be swapped, we swap and increment the pointer.

+++

Result:

    Submission accepted at 32 ms.


"""

class Solution:
    def sortThreeColors(self, nums):
        zeroP = oneP = 0
        for i in range(len(nums)):
            if nums[i] == 0:
                nums[i], nums[zeroP] = nums[zeroP], nums[i]
                zeroP += 1
                if zeroP > oneP:
                    oneP += 1
            if nums[i] == 1:
                nums[i], nums[oneP] = nums[oneP], nums[i]
                oneP += 1

class Solution2:
    def sortThreeColors(self, nums):
        """ This uses a different pointer - to demonstrate that it can be done. """
        zeroP, twoP = 0, len(nums)-1
        while i <= twoP:
            if nums[i] == 0:
                swap(zeroP, i)
                zeroP += 1
            if nums[i] == 2:
                swap(twoP, i)
                twoP -= 1
                # this is tricky part here.
                # we could have moved something here that needs to be checked
                # again. thus, we do not increment i but repeat the process.
                continue
            i += 1


