""" 94. Binary Tree Inorder Traversal

Question:

    Given a binary tree, return the inorder traversal of its nodes' values.

+++

Attempt:

    The in-order traversal in recursive algorithm is rather trivial. If we are
    to approach this iteratively, we could implement DFS iteratively.

+++

Result:

    Submission accepted at 28 ms. Neat trick by making a tuple of (node,
    status). Also, iterative DFS + stack operations - we push right first before
    left.

"""

class Solution:
    def inorderTraversal(self, root):
        result = []
        stack = [ (root, False) ]
        while stack:
            curr, visited = stack.pop()
            if curr:
                if visited:
                    result.append(curr.val)
                else:
                    if curr.right:
                        stack.append( (curr.right, False) )
                    stack.append( (curr, True) )
                    if curr.left:
                        stack.append( (curr.left, False) )
        return result
