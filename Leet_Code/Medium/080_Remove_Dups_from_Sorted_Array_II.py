""" 80. Remove Duplicates from Sorted Array II

Question:

    Given a sorted array nums, remove the duplicates in-place such taht
    duplicates appeared at most twice and return the new length.

+++

Attempt:

    The fact that array is already in sorted order suggests that all the
    duplicates - if they appear - will be next to each other. We can keep two of
    same elements but not any more beyond that.

    Thus, we prepare an insert pointer that will indicate the valid insertion
    point. If the value at scan is not same as value at insertion point, then it
    can be safely inserted. Otherwise, if it is same value, then it should not
    be same as the value before the insertion point. Let i be insertion point,
    then...

        for j in range(2, len(nums)):
            if nums[i] != nums[j] or (nums[i] == nums[j] and nums[i-1] !=
            nums[j]):
                swap ...

    We can think of another way: the first two values are ok by base case. Then,
    start swapping from index 2. The new value then must not be equal to the
    previous pair of duplicates (index-2).

+++

Result:

    Submission accepted at 56 ms.

"""

class Solution:
    def removeDuplicates(self, nums):
        if not nums or len(nums) <= 2:
            return nums
        i = 2
        for j in range(2, len(nums)):
            if nums[i-2] != nums[j]:
                nums[i], nums[j] = nums[j], nums[i]
                i += 1
        return i

