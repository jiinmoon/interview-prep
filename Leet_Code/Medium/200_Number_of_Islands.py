""" 200. Number of Islands

Question:

    Given a 2D grid map of '1's and '0's, count the number of islands. Assume
    that all four edges of the grid are all surrounded by water.

+++

Attempt:

    Backtracking algorithm would do. As we traverses on the lands we mark them
    visited, and then move on as far as lands there are. Then, repeat on another
    starting position while increasing total island.

+++

Result:

    Submission accepted at 144 ms.

"""

class Solution:
    def numIslands(self, grid):
        totalIslands = 0
        m, n = len(grid), len(grid[0])

        def isLand(x, y):
            return grid[x][y] == '1'

        def inBound(x, y):
            return x >= 0 and x < m and y >= 0 and y < n

        # dfs.
        def findLands(x, y):
            stack = [ (x, y) ]
            while stack:
                x, y = stack.pop()
                # mark the curr coords visited.
                grid[x][y] = float('-inf')
                for i, j in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
                    if inBound(i, j) and isLand(i, j):
                        stack.append(i, j)

        for x in range(m):
            for y in range(n):
                if isLand(x, y):
                    totalIslands += 1
                    findLands(x, y)

        return totalIslands
