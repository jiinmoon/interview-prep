""" 173. Binary Search Tree Iterator

Question:

    Implement an iterator over a BST such that its next() function will return
    the smallest number in the BST.

+++

Attempt:

    There are several ways to approch this problem, with their own advantages
    and disadvantages. One way is to traverse the entire tree to store the
    values in the sorted order such that we can return them as soon as possible.
    Not the most efficient idea in terms of space, but precomuting like this
    does work nicely when the data base will be used heavily for access reasons.

    Another approach would be to we traverse when needed - we maintain our
    current position in the tree via stack. Then, simple traversal to populate
    the stack, only when needed. For example, at beginning, we traverse all the
    way down to the leftmost node which would be the min of the tree. From here,
    whenever next() is called, we pop from stack. This would be the node to be
    returned. But we also have to find the its right child's leftmost node which
    is the next minimum value to be returned; and store all the nodes along the
    path in the stack.

+++

Result:

    Submission accepted at 72 ms.

"""

class BSTIter:

    def __init__(self, root):
        self.stack = []
        self._findLeftMostNode(root)

    def _findLeftMostNode(self, node):
        while node:
            self.stack.append(node)
            node = node.left

    def next(self):
        curr = self.stack.pop()
        # we are currently at left most node.
        # now we need to find 'next' node; and record path taken to stack.
        self._findLeftMostNode(curr.right)
        return curr.val

    def hasNext(self):
        return len(self.stack) != 0

