""" 64. Minimum Path Sum

Question:

    Given a m x n grid filled with positive integers, find a path from top left
    to bottom right which minimizes the sum of all numbers along its path.

+++

Attempt:

    The naive approach of use backtracking algorithm to find all the possible
    path and compare their path sum is a brute force algorithm that is less than
    satisfactory.

    We can view this as a graph problem - where we find the min path between
    given two nodes. We could use a Dijkstra's algorithm or else, but honestly,
    set up alone is a nightmare to deal with.

    Then, we can think of a simple dynammic programming approach. Let's
    generalize the problem - to arrive at any single tile at this grid, it would
    have costed us minimum of either left or up tiles' costs and the cost
    itself. In other words...

    dp[i][j] = min(dp[i-1][j], dp[i][j-1]) + mat[i][j]

+++

Result:

    Submission accepted at 100 ms. There are optimization possible (could have
    used the same grid instead of dp duplicate to save memory), but I wanted to
    display the logic clearly.

"""

class Solution:
    def minPathSum(self, grid):
        m, n = len(grid), len(grid[0])
        dp = [[0 for _ in range(n)] for _ in range(m)]

        # set up first row and col.
        # it is obvious that we only have one direction to go in these cases.
        dp[0][0] = grid[0][0]
        for i in range(1, n):
            dp[0][i] = dp[0][i-1] + grid[0][i]

        for j in range(1, m):
            dp[j][0] = dp[j-1][0] + grid[j][0]

        # start dp
        for i in range(1, m):
            for j in range(1, n):
                dp[i][j] = min(dp[i-1][j], dp[i][j-1]) + grid[i][j]
        return dp[-1][-1]



