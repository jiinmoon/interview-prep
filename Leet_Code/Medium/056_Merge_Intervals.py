""" 56. Merge Intervals

Question:

    Given a collection of intervals, merge all overlapping intervals.

+++

Attempt:

    Since we do not have a gurantee that the intervals [i, j] are in sorted
    order, we conceed and sort by their first value to give some ordering. Once
    we have the list of sorted intervals, then it is a matter of going through
    the list of intervals, checking the end value of what is in the result array
    against the start of the current interval encountered. If it is overlap, we
    just update the end of whatever is in the result list, otherwise, we append
    the new interval.

+++

Result:

    Submission accepted at 84 ms. For unknown reason, the memory was a bit of
    issue, only better than 6.52%.

"""

class Solution:
    def merge(self, intervals):
        if not intervals or len(intervals) <= 1: return intervals
        intervals.sort(key = lambda x: x.start)
        result = [intervals[0]]
        for interval in intervals[1:]:
            if result[-1].end < interval.start:
                result.append(interval)
            else:
                result[-1].end = max(result[-1].end, interval.end)
        return result
