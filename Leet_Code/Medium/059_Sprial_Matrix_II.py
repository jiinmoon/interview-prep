""" 59. Spiral Matrix II

Question:

    Given a positive integer n, generate a square matrix filled with elements
    from 1 to n**2 in spiral order.

+++

Attempt:

    Previously, we have create a generator function to yield the coords given
    the rows and cols. Hence, we can repeat the same process.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def generateMatrix(self, n):
        def spiralCoords(r1, r2, c1, c2):
            # move right on top row.
            for col in range(c1, c2+1):
                yield r1, col
            # move down on rightmost col.
            for row in range(r1+1, r2+1):
                yield row, c2
            if r1 < r2 and c1 < c2:
                # move left on bottom row.
                for col in range(c2-1, c1, -1):
                    yield r2, col
                # move up on leftmost col.
                for row in range(r2, r1, -1):
                    yield row, c1

        result = [[0 for _ in range(n)] for _ in range(n)]
        r1 = c1 = 0
        r2 = c2 = n - 1
        i = 1
        while r1 <= r2 and c1 <= c2:
            for row, col in spiralCoords(r1, r2, c1, c2):
                result[row][col] = i
                i += 1
            r1 += 1; c1 += 1;
            r2 -= 1; c2 -= 1;
        return result
