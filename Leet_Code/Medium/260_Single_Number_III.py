""" 260. Single Number III

Question:

    Given an array of numbers nums in which exactly two elements appear only
    once and all the other elements appear exactly twice. Find the two elements
    that ppear only once.

+++

Attempt:

    2A + 2B + 2C + ... + X + Y = sum(nums)

    The obvious solution here would be to count the elements to find the single
    numbers. If set() structure is allowed, we can use set operations to find
    the elements as well.

+++

Result:

    Submissioin accepted at 64 ms. Not very satisfying but does the job
    wonderfully.

"""

from collections import defaultdict

class Solution:
    def singleNumber(self, nums):
        record = defaultdict(int)
        for num in nums:
            record[num] += 1
            if record[num] > 1:
                del record[num]
        return list(record.keys())
