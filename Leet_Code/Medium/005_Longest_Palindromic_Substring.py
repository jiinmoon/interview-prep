""" 5. Longest Palindromic Substring

Question:

    Given a string s, find the longest palindromic substring in s.

+++

Attempt:

    Naive approach would be to consider every substring, then check whether the
    substring is a palindrome or not. This would result in O(n^3) as it takes
    O(n^2) to consider every substring, and takes O(n) time to check for
    palindrome.

    Improvement would be to utilize the DP definition, we know that for any
    given substring i, j to be palindrome, we can check previous case i+1, j-1
    and see whether new char at i and j are same.

    Another method would be expand the substring from given index. We would have
    to account for even and odd number of cases.

+++

Result:

    Submission accepted at 1004 ms. Had a difficulty at what to return at expand
    function. Originally thought that it should have been s[start:end+1].

"""

class Solution:

    def expand(self, s, start, end):
        while start >= 0 and end < len(s) and s[start] == s[end]:
            start -= 1
            end += 1
        return s[start+1:end]

    def longestPalindrome(self, s):
        maxS = ''
        for i in range(len(s)):
            s1 = self.expand(s, i)
            s2 = self.expand(s, i+1)
            currMax = s1 if len(s1) > len(s2) else s2
            maxS = currMax is len(currMax) > len(maxS) else maxS
        return maxS
