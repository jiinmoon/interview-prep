""" 3. Longest Substring Without Repeating Characters

Question:

    Given a string, find the length of the longest substring without repeating
    characters.

+++

Attempt:

    We maintain two variables: since we looking for the length of the longest
    substring, we maintain maxLengthThusFar variable, and current substring. As
    we iterate chars on the given string, we build the current substring. If it
    is a repeat, then we start over from the where the repeat begins, otherwise,
    we simply append, buidling on the current substring. And then, the maximum
    length is considered.

+++

Result:

    Submission accepted at 88 ms.

"""

class Solution:

    def lengthOfLongestSubstring(self, s):
        if not s: return 0
        if len(s) == 1: return 1
        maxThusFar, currSubstr = 0, []
        for char in s:
            if char in currSubstr:
                currSubstr = currSubstr[currSubstr.index(char) + 1:] + [char]
            else:
                currSubstr += [char]
            maxThusFar = max(maxThusFar, len(currSubstr))
        return maxThusFar
