""" 2. Add Two Numbers

Question:

    Given two non-empty linked lists representing two non-negative integers. The
    digits are stored in revers eorder and each of their nodes contain a single
    digit. Add two numbers and return it as a linked list.

+++

Attempt:

    We do not have to devise a complicated algorithm to try to modify and linked
    list as we iterate. Simply, we would create few helper functions that will
    convert list to integer, and we would convert integer back into the list.

+++

Result:

    Submission accepted at 68 ms.

 """

class Solution:

    def listToInt(self, head):
        result = i = 0
        while head:
            result += head.val * 10 ** i
            i += 1
            head = head.next
        return result

    def intToList(self, num):
        dummyHead = curr = ListNode(-1)
        dummyHead.next = curr
        while num:
            curr.next = ListNode(num % 10)
            num //= 10
        return dummyHead.next

    def addTwoNums(self, l1, l2):
        # two lists are non-empty.
        result = self.listToInt(l1) + self.listToInt(l2)
        return self.intToList(result) if result > 0 else ListNode(0)
