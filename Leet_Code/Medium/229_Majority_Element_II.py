""" 229. Majority Element II

Question:

    Given an integer array of size n, find all elements that appear more than
    floor(n/3) times. It should run in linear time and O(1) space.

+++

Attempt:

    The interesting twist here is that we need to find multiple elements that
    appear at least third of the list.

    Given that the algorithm should run in linear time, we cannot sort the
    array. And space limitation does not allow us to record the elements that we
    encounter either.

    [1, 1, 1, 2, 2, 2, 3, 3]

    { 1: 1 }
    { 1: 2 }
    { 1: 3 }
    { 1: 3, 2: 1 }
    { 1: 3, 2: 2 }
    { 1: 3, 2: 3 }
    { 1: 2, 2: 2 }
    { 1: 1, 2: 1 }

    We use modified BM's majority vote algorithm. Note that there can be only
    two answers in this restriction (n//3). Thus, we only have to keep counting
    the two variables. Whenever we encounter a value for first time, we keep
    track of it. But whenever other variable is encountered we decrement them.

+++

Result:

    Submission accepted at 132 ms.

"""

class Solution:
    def majorityElement(self, nums):
        if not nums: return []
        result = []
        first, second = 0, 1
        firstCount = secondCount = 0
        for num in nums:
            if first == num:
                first += 1
            elif second == num:
                second += 1
            elif firstCount == 0:
                first = num
                firstCount = 1
            elif secondCount == 0:
                second = num
                secondCount = 1
            else:
                firstCount -= 1
                secondCount -= 1
        # these are majority candidates. but do they go over threshold of n//3 ?
        firstCount = secondCount = 0
        for num in nums:
            if num == first:
                firstCount += 1
            if num == second:
                secondCount += 1
        if firstCount > len(nums) // 3:
            result.append(first)
        if first != second and secoundCount > len(nums) // 3:
            result.append(second)
        return result

