""" 162. Find Peak Element

Question:

    A peak element is an element that is greater than its neighbours.

    Given an input array where its neighbours won't be duplicates, find a peak
    element and return its index.

+++

Attempt:

    Before examining all the elements, we would not necessarily know that there
    exists a higher peak value - thus, this is a O(n) minimum time complexity.

    But this is a trap thinking - think not of singluar elements, but trend. At
    any given point, we are either in ascending or decending slope. Important to
    note here is that we are not here to find the global maximum; all we are
    interested is any one of the local minimum.

    If so, then we can simply use binary search. At every iteration, we follow
    the rising half of the trend.

+++

Result:

    Submission accepted at 72 ms.

"""

class Solution:
    def findPeakElement(self, nums):
        lo, hi = 0, len(nums) - 1
        while lo < hi:
            mid = lo + (hi - lo) // 2
            if nums[mid] < nums[mid+1]:
                lo = mid + 1
            else:
                hi = mid
        return lo
