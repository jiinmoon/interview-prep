""" 130 Surrouneded Regions

Question:

    Given a 2D board containing 'X' and 'O', capture all regions surrounded by
    'X'.

+++

Attempt:

    The important to note is that the surrounded regions shouldn't be on the
    border. Thus, we start search algorithm at every zeros that are not touching
    the O.

    In short, we think more simply: assume that we flip unless they touch the
    borders. Hence, we prepare a set to collect all the coordinates of zeroes
    that should be flipped.

    I can think of backtracking as a solution. Either DFS or BFS, but the idea
    is same, first traverse to mark the places of 'O's which are on border or
    touching it. Then, traverse again to fliped them.

+++

Result:

    First solution was unique so tried out but it is not
    substentially fast. It is acceptd at 152 ms and used up too much memory.

    A simple DFS backtrack approach was slightly faster at 144 ms, but memory have
    improved a lot.

"""

class Solution:

    def solve(self, board):
        # traverse the board to find coords of 'O's as (i, j).
        # also, record whether it is touching the border.
        m = len(board)
        if not m:
            return
        n = len(board)
        zeroInBorder = set() # accepts only immutables
        zeroInNonBorder = set()
        for i in range(m):
            for j in range(n):
                if board[i][j] == 'O':
                    if (i == 0 or j == 0 or i == m-1 or j == n-1):
                        zeroInBorder.add( (i, j) )
                    else:
                        zeroInNonBorder.add( (i, j) )

        # start BFS to remove those that touches the border 'O's.
        queue = list(zeroInBorder)
        while queue:
            (i, j) = queue.pop(0)
            for (x, y) in [ (i+1, j), (i-1, j), (i, j+1), (i, j-1) ]:
                # remember that all the starting points are 'O's in borders.
                # thus, everything we found to be in neighbour are to be
                # flipped - and added in queue for further exploration.
                if (x >= 0 and x < m) and (y >= 0 and y < n) and \
                    board[x][y] == 'O' and (x, y) not in zeroInBorder:
                    zeroInNonBorder.remove((x, y))
                    zeroInBorder.add((x, y))
                    queue.append((x, y))

        for i, j in zeroInNonBorder:
            board[i][j] = 'X'


class Solution2:

    def solve(self, board):
        m = len(board)
        if not m:
            return
        n = len(board[0])

        def backtrack(i, j):
            board[i][j] = ' '
            for (x, y) in [ (i+1, j), (i-1, j), (i, j+1), (i, j-1) ]:
                if x >= 0 and x < m and y >= 0 and y < n and board[x][y] == 'O':
                    backtrack(x, y-1)

        # backtrack on border 'O's at first/lasw rows.
        for i in range(n):
            if board[0][i] == '0':
                backtrack(0, i)
            if board[m-1][i] == '0':
                backtrack(m-1, i)

        # backtrack on border 'O's at first/last cols.
        for i in range(m):
            if board[i][0] == 'O':
                backtrack(i, 0)
            if board[i][n-1] == '0':
                backtrack(i, n-1)

        for i in range(m):
            for j in range(n):
                if board[i][j] == 'O':
                    board[i][j] = 'X'
                if board[i][J] == ' ':
                    board[i][j] = 'O'
