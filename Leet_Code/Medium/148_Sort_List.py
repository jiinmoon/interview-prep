""" 148. Sort List

Question:

    Sort a linked list in O(n lg n) time using constant space complexity.

+++

Attempt:

    There are many sorting algorithms to choose from - but in this case, we have
    to implement one that works in-place. Merge sort is one that fits the bill -
    and works perfectly with ListNodes.

    For refresher, merge sort is similar to quicksort in that it is a divide and
    conquer algorithm. It constantly divides the list into two halves until they
    are at their element, then up the way up, merges two - comparing two
    elements.

    mergeSort(arr, l, r)
        if r > l:
            mid = (l + r) // 2
            l1, l2 = arr[:mid], arr[mid:]
            mergeSort(l1, l, mid)
            mergeSort(l2, mid, r)
            merge(arr, l, m, r)

+++

Result:

    Submission accepted at 440 ms. The first solution appears to be not as good
    as expected it to be.

"""

class Solution:
    def merge(self, bottom, upper):
        # merge two given lists, but compare two values to see whichever one
        # comes first.
        dummyHead = curr = ListNode(float('-inf'))
        while bottom and upper:
            if bottom.val < upper.val:
                curr.next = bottom
                bottom = bottom.next
            else:
                curr.next = upper
                upper = upper.next
            curr = curr.next
        curr.next = bottom or upper
        return dummyHead.next


    def sortList(self, head):
        # merge sort the list.
        if not head or not head.next: return head

        # find mid.
        slow, fast = head. head.next
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
        head2 = slow.next
        slow.next = None
        # recursively sort on two halves.
        bottomList = self.sortList(head)
        upperList = self.sortList(head2)
        head = self.merge(bottomList, upperList)
        return head
