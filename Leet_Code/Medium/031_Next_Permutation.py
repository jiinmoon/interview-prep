""" 31. Next Permutation

Question:

    Given an arrangement of integers, find the next greater permutation of
    numbers.

+++

Attempt:

    The naive approach would be to simply generate all the possible permutation
    to find the next case. The O(n) algorithm requires a deep thinking on the
    matter.

    Firstable, if the every sequence is in descending order, no next larger
    permutation is possible (other than reversing it and start over).

    Hence, we find the first two numbers that a[i] > a[i-1]. This indicates the
    starting point where the larger permutation can be found. And it needs to be
    larger than it, a[i-1] needs to be swapped with first value larger than
    itself on the sequence starting from itself.

    Then, the entire sequence starting from i is reversed to start the
    permutation.

    Few practices with sample arrays is required.

+++

Result:

    Submission accepted at 44 ms.


"""

class Solution:
    def reverse(self, nums, start):
        end = len(nums) - 1
        while start < end:
            nums[start], nums[end] = nums[end], nums[start]
            start += 1
            end -= 1

    def nextPermutation(self, nums):
        # start search to find first non-decending value.
        i = len(nums)-2
        while i >= 0 and nums[i] >= nums[i+1]:
            i -= 1

        # if found, find next greater value starting from it.
        # then perform swap.
        if i >= 0:
            j = len(nums) - 1
            while j >= 0 and nums[j] <= nums[i]:
                j -= 1
            nums[i], nums[j] = nums[j], nums[i]
        self.reverse(nums, i+1)
