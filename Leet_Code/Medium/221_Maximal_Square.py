""" 221. Maximal Square

Question:

    Given a 2D binary matrix filled with 0's and 1's, find the largest square
    containing only 1's and return its area.

+++

Attempt:

    How do we approach a problem such as this? Suppose that we are at any given
    grid, then how do we know that we are at a square? This can be done by
    comparing 3 previous tiles. Suppose we are at (i, j) - then we check
    (i-1,j), (i,j-1), and (i-1, j-1). This can tell us that we are at a square.

    This can propagate so long as we have a square - if not, one of them will be
    0 resetting the process. So, we also maintain another maxThusFar variable
    which indicates the maximum length of the square we have seen.

+++

Result:

    Submission accpeted at 216 ms.

"""

class Solution:
    def maximalSquare(self, matrix):
        if not matrix: return 0
        maxThusFar = 0
        m, n = len(matrix), len(matrix[0])
        for i in range(m):
            for j in range(n):
                matrix[i][j] = int(matrix[i][j])
                if i > 0 and j > 0 and matrix[i][j]:
                    matrix[i][j] = min(matrix[i-1][j], matrix[i][j-1], matrix[i-1][j-1]) + 1
                maxThusFar = max(maxThusFar, matrix[i][j])
        return maxThusFar * maxThusFar

class Solution2:
    def maximalSquare(self, mat):
        if not mat: return 0
        m, n = len(mat), len(mat[0])
        maxThusFar = 0
        dp = [0] * (n + 1)
        prev = 0

        for i in range(1, m+1):
            for j in range(1, n+1):
                temp = dp[j]
                if mat[i-1][j-1] == '1':
                    dp[j] = min(dp[j], dp[j-1], prev) + 1
                    maxThusFar = max(maxThusFar, dp[j])
                else:
                    dp[j] = 0
                prev = temp
        return maxThusFar * maxThusFar
