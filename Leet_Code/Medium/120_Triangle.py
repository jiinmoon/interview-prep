""" 120. Triangle

Question:

    Given a triangle, find the min path sum from top to bottom. Each step you
    may move to adjacent numbers on the row below.

+++

Attempt:

    We could use backtracking to explore all paths to find the minimum,
    returning the min fonud from previous depth. But simplest approach here is
    to think of this problem as a dp. To reach any triangle cell, it is the
    accumulation of the path sum to took there. Thus, we define traingle cell as
    a minPathThusFar. We can build from top - down or bottom - up.

+++

Result:

    Submission accepted at 60 ms. It says that memory usage is less than 20 % of
    the submissions, but I am not sure why. I worked with given...in-place.

"""

class Solution:
    def minTotal(self, triangle):
        if not triangle: return 0
        if len(triangle) > 1:
            or i in range(len(triangle)-2, -1, -1):
               # prevRow = triangle[i+1]
               # currRow = triangle[i]
               # for every value in currRow, it comes from its min between
               # t[index], and t[index + 1]) of previous row.
               for j in range(len(triangle[i])):
                   triangle[i][j] = triangle[i][j] + \
                                   min(triangle[i+1][j]) + triangle[i+1][j+1]
        return triangle[0][0]

