""" 81. Search in Rotate Sorted Array

Question:

    Suppose an array sorted in ascending order is rotated at some pivot unknown
    to you beforehand. You are given a target value to search.

+++

Attempt:

    Simple modified binary search method would do.

+++

Result:

    Submission accepted at 56 ms. Not a fun problem to solve - it is really
    all about adjusting operator signs and +1s.

"""

class SolutioN:
    def search(self, nums, target):
        lo, hi = 0, len(nums)-1

        while lo <= hi:
            mid = lo + (hi - lo) // 2
            if nums[mid] == target:
                return True
            # nums[mid] is not target, yet it can be same as nums[lo].
            if nums[mid] == nums[lo]:
                lo += 1
            # check whether bottom or upper half is sorted.
            if nums[lo] < nums[mid]:
                # is target within bottom?
                if nums[lo] <= target and target <= nums[mid]:
                    hi = mid
                else:
                    lo = mid + 1
            else:
                if nums[mid] <= target and target < nums[hi]:
                    lo = mid + 1
                else:
                    hi = mid

        return False
