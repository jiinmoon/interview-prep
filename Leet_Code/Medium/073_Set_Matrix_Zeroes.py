""" 73. Set Matrix Zeroes

Question:

    Given a m x n matrix, if an element is 0 set its entire row and col to 0.

+++

Attempt:

    We cannot simply iterate and change every row and col to zero since it will
    overwrite the future information about whether to zero the other rows and
    cols. Hence, we would first record the rows and cols to be zero'd out.

+++

Result:

    Submission accepted at 140 ms. May try to be more clean, and factorize the
    functions - such as zeroRow or zeroCol.

"""

class Solution:
    def setZeroes(self, matrix):
        m, n = len(matrix), len(matrix[0])

        # is first row and col needs to be zeroed in the end?
        # this is because we will need to use first row/col as a record.
        zeroFirstRow = 0 in matrix[0]
        zeroFirstCol = 0 in [matrix[i][0] for i in range(m)]

        # iterate to mark the zeros.
        for i in range(1, m):
            for j in range(1, n):
                if not matrix[i][j]:
                    matrix[i][0] = 0
                    matrix[0][j] = 0

        # zero the rows and cols as inidcated.
        for i in range(1, m):
            for j in range(1, n):
                if not matrix[i][0] or not matrix[0][j]:
                    matrix[i][j] = 0

        if zeroFirstRow:
            for i in range(n):
                matrix[0][i] = 0

        if zeroFirstCol:
            for i in range(m):
                matrix[i][0] = 0


