""" 43. Multiply Strings

Question:

    Given two non-negative integers represented as strings, return the product
    of two which is also represented as a string.

+++

Attempt:

    Since the question does not allow us to us any built-in BigInteger or
    convert the inputs to integer directly, it is asking us about how strings
    are represented underneath - and how ASCII or encodings work. Hence, we
    implement our own strToInt function.

+++

Result:

    Submission accepted at 32 ms. Encoding is interesting topic to take a look
    at whenever I get a chance.

"""

class Solution:
    def strToInt(self, num):
        result = 0
        for digit in num:
            result = result * 10 + ord(digit) - ord('0')
        return result

    def multiply(self, num1, num2):
        product = self.strToInt(num1) * self.strToInt(num2)
        return str(product)

