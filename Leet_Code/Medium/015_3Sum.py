""" 15. 3Sum

Question:

    Given an array of n integers, are there elements a, b, c in nums such that
    they add up to 0? Find all the unique triplets in the array.

+++

Attempt:

    Naive solution would be to consider every possible combinations. But once we
    sort the array, we can have a directionality as to how to navigate the
    pointers within the nums when we are choosing the values for a, b, and c.

    It requires outter loop to choose the values of a. Once a is chosen, we set
    up two pointers b and c which are placed at each end to consider values. We
    know how to move the pointers now since if they sum to value greater than 0
    then we know higher pointer should move towards smaller value. And vice
    versa.

+++

Result:

    Submission accepted at 1132 ms. The difficulty of the problem is with making
    sure we are choosing only the unique triplets.

"""

class Solution:
    def threeSum(self, nums):
        if not nums or len(nums) <= 2: return []
        nums.sort()
        result = []
        for i in range(len(nums)-2):
            # skip duplicate choices for a.
            if i != 0 and nums[i-1] == nums[i]:
                continue
            j = i + 1
            k = len(nums)-1
            while j < k:
                a, b, c = nums[i], nums[j], nums[k]
                if a + b + c == 0:
                    result.append([a, b, c])
                    j += 1
                    k -= 1
                    # potential duplicates exist for b and c.
                    # we want only unique triplets.
                    while j < k and nums[j-1] == nums[j]:
                        j += 1
                    while j < k and nums[k+1] == nums[k]:
                        k -= 1
                elif a + b + c < 0:
                    j += 1
                else:
                    k -= 1
        return result
