""" 48. Rotate Image

Question:

    You are given an n x n 2D matrix representing an image.

    Rotate the image by 90 degrees clockwise.

+++

Attempt:

    There are multiple ways to approach this problem. You can apply linear
    algebra to rotate the matrix (which requires matrix multiplication), or
    figure out the coords, and swap them in-place.

    But best way would be to realize that to rotate the image, we can first
    reverse the rows, then swap the values across.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:

    def rotate(self, matrix):
        matrix.reverse()
        n, m = len(matrix), len(matrix[0])
        for i in range(n):
            for j in range(i+1, m):
                matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]
