""" 147. Insertion Sort List

Question:

    Sort a linked list using insertion osrt.

+++

Attempt:

    This is rather a trap question: at first glance, you may want to save time
    and space, so you will pain stakingly trying to sort the list nodes. But you
    have to understand that copying the values, sort, then returning them again
    does not cost you much in terms of time complexity - as copying operation is
    at O(n). Only question is whether we can afford the O(n) space.

+++

Result:

    Submission accepted at 44 ms. Weirdly enough, the memory usage is actually
    less than 100% than other submissions?

"""

class Solution:
    def insertionSortList(self, head):
        if not head or not head.next: return head

        temp = []
        curr = head
        while curr:
            temp.append(curr.val)
            curr = curr.next

        # insert your fav. sort function...
        # python sort is already efficient as it is.
        temp.sort()
        curr = head
        for val in temp:
            curr.val = val
            curr = curr.next

        return head
