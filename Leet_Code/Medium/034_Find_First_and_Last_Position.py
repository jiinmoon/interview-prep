""" 34. Find First and Last Position of Element in Sorted Array

Question:

    Given an array of integers nums sorted in ascending order, find the starting
    and ending position of a given target value.

+++

Attempt:

    Again, as the array is sorted, we may use the binary search algorithm -
    however, we do not stop when we have mid == target, but continue on until we
    find the left most occurrence of that value. Then, to find last position, we
    would perform another search on target + 1.

+++

Result:

    Submission accepted at 84 ms. There is a really weird +1 error at
    determining mid value in bisectRight. The mid value had to be skewd to the
    right by 1 each time...

"""

class Solution:
    # the important to notice the difference between bisectLeft and Right is
    # that the order of if statements. The ordering is the reason why left
    # function will always move left first and right vice versa.
    def bisectLeft(self, nums, target):
        lo, hi = 0, len(nums)-1
        while lo < hi:
            mid = (lo + (hi - lo)) // 2
            if nums[mid] < target:
                lo = mid + 1
            else:
                hi = mid
        return lo if nums[lo] == target else -1

    def bisectRight(self, nums, target):
        lo, hi = 0, len(nums)-1
        while lo < hi:
            mid = (lo + (hi - lo) + 1) // 2
            if nums[mid] > target:
                hi  = mid -1
            else:
                lo = mid
        return lo if nums[lo] == target else -1

    def searchRange(self, nums, target):
        leftIdx = self.bisectLeft(nums, target)
        rightIdx = self.bisetRight(nums, target)
        return [leftIdx, rightIdx]
