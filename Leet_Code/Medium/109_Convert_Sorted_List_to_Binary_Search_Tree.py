""" 109. Convert Sorted List to Binary Search Tree

Question:

    GIven a singly linked list where elements are sorted in ascending order,
    convert it to a height balanced BST.

+++

Attempt:

    Suppose that it was not linked list but a simple sorted list. Then, to build
    the height balanced binary search tree, we would choose the mid element to
    be our node, and divide the list into lower and upper halves, which will
    contain the elements for left and right subtrees. This will repeat itself
    for over recursively.

    But with singly linked list, we have an added complexity of finding/getting
    to the mid element at each step.

+++

Result:

    Submission accepted at 132 ms.

"""

class Solution:
    def sortedListToBST(self, head):
        # base case: no node left or single case.
        if not head or not head.next:
            return TreeNode(head.val) if head else None

        # find mid element in the linked list.
        # here we use a trick - two pointers with one moving faster than other.
        # slow will be at mid.
        slow, fast = head, head.next
        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next

        # now, split list into halves.
        head2 = slow.next
        slow.next = None

        # recursively call to build on left and right childs.
        node = TreeNode(head2.val)
        node.left = self.sortedListToBST(head) # lower half of the list.
        node.right = self.sortedListToBST(head2.next) # upper half (-mid node).

        return node

