""" 82. Remove Duplicates from Sorted List II

Question:

    Given a sorted linked list, delete all nodes that have duplicate numbers,
    leaving only distinct numbers from the original list.

+++

Attempt:

    This is a problem best approached by using multiple pointers. We move in
    pairs - that is until we have curr and curr.next.

    If we detect a duplicates ahead, then we start a scan to see how far the
    duplicates extends. Then, we rewire our curr node to next non-duplicate
    node.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def deleteDuplicates(self, head):
        dummyHead = ListNode(float('-inf'))
        dummyHead.next = head
        curr = dummyHead.next
        prev = dummyHead
        while curr and curr.next:
            if curr.val == curr.next.val:
                temp = curr.val
                while curr and curr.val == temp:
                    curr = curr.next
                prev.next = curr
            else:
                prev = curr
                curr = curr.next
        return dummyHead.next
