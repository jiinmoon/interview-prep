""" 211. Add and Search Word

Question:

    Design a data structure that supports addWord and searchWord. It should
    suuport a regular expression '.' which is a wild card.

+++

Attempt:

    One of efficient data struct for these type of operations would be using
    Trie structure. The problem is that how are we to deal with the wild card
    character. This complication has to be dealt by backtracking algorithm
    during search. Whenever we encounter wild card, we need to start search on
    every child of the given node.

+++

Result:

    Submission accepted at 320 ms.

"""

from collections import defaultdict

class TrieNode:
    def __init__(self):
        self.children = defaultdict(TrieNode)
        self.isWord = False

class Solution:
    def __init__(self):
        self.root = TrieNode()

    def addWord(self, word):
        trie = self.root
        for char in word:
            if char not in trie:
                trie = trie.children[char]
        trie.isWord = True

    def search(self, word):
        def _search(node, pattern, index):
            if not node: return False
            if index == len(pattern):
                return node.isWord
            currChar = pattern[index]
            if currChar == '.':
                for child in node.children.values():
                    if _search(child, pattern, index+1):
                        return True
                    return False
            return _serach(node.children[currChar], pattern, index+1)
        return _search(self.root, word, 0)
