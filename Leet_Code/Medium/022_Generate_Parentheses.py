""" 22. Generate Parentheses

Question:

    Given n pairs of parentheses, wrtie a function to generate all combinations
    of well-formed parentheses.

+++

Attempt:

    To explore all combinations, we can use back-tracking algorithm.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def generateParenthesis(self, n):
        if n == 0: return []
        result = []

        def backtrack(pairCount, openCount, path):
            # base case. no more parentheses to generate.
            if pairCount + openCount == 0:
                result.append(path)
                return
            # any open parentheses need to be closed?
            if openCount > 0:
                path += ')'
                backtack(pairCount, openCount-1, path)
                # when done, restore path to explore another path.
                # think stack.
                path = path[:-1]
            # if more pair to consider?
            if pairCount > 0:
                path += '()'
                backtrack(pairCount - 1, openCount + 1, path)
                path = path[:-1]

        backtrack(n, 0, '')
        return result
