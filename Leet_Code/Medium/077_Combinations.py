""" 77. Combinations

Question:

    Given positive integer n and k, return all possible combinations of k
    numbers out of range(n).

+++

Attempt:

    A typical approach to this problem would be using backtracking algorithm to
    generate the all combinations possible.

+++

Result:

    Submission accepted at 540 ms. What a large runtime, but this is better than
    ~60% of the submissions. I wonder what the better version of the solutions
    are? Do they use some other lib functions?


"""

class Solution:
    def combine(self, n, k):
        result = []

        def backtrack(start, path):
            if len(path) == k:
                result.append(list(path))
                return
            for i in range(start, n+1):
                path.append(i)
                backtrack(i+1, path)
                path.pop()

        # careful, start is not 0: it is not 0 index'd.
        backtrack(1, [])
        return result
