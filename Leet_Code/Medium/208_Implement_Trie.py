""" 208. Implement Trie

Question:

    Implement a trie with insert, search, and startsWith methods.

+++

Attempt:

    There are many ways to implement the trie - we could actually create a trie
    nodes and make a tree-like structure like its other name suggests (Prefix
    Tree). But we may use another highly efficient data structue to support the
    Trie ADT - hashmap. Imagine each node as a dictionary, that contains char.
    And it is nested such that looks like follows:

        { a:
            { p:
                { p:
                    { .... }
                }
            },

            { t:
                {...}
            },
        }.

+++

Result:

    Submission accepted at 124 ms. Simple implementations are the best.

"""

class Trie:
    def __init__(self):
        self.root = {}

    def insert(self, word):
        trie = self.root
        for char in word:
            if char not in trie:
                trie[char] = {}
            trie = trie[char]
        # we need a way to tell whether this is a word at this depth.
        trie['+'] = word

    def search(self, word):
        trie = self.root
        for char in word:
            if char not in trie:
                return False
            trie = trie[char]
        return '+' in trie

    def startsWith(self, prefix):
        trie = self.root
        for char in prefix:
            if char not in trie:
                return False
            trie = trie[char]
        return True
