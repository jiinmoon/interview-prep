""" 187. Rpeated DNA Sequences

Question:

    All DNA is composed of nucleotides, A, C, G, and T. Write a funtion to find
    all the 10-letter-long sequences that occur more than once in a DNA
    moelecule.

+++

Attempt:

    A pattern recognition algorithm. There are multiple finding needle in
    haystack algorithms out there such as KMP, and so on.

    But implemeneting these full scale algorithm is a dounting task - even more
    so if you are to implement it from just memory. Thus, the simplest way
    would be to use sliding window - of 10-char length, and use hashmap to check
    whether we have seen it.

+++

Result:

    Submission accepted at 64 ms. This is not the most elegant solution. We
    could hash the sequences such that we can save the memory, or try different
    data structures for record. For example, it appears that using set() instead
    helps improving the memory somewhat.

"""

class Solution:
    def findDNA(self, s):
        record = {}
        result = []
        for i in range(len(s)-9):
            currSeq = s[i:i+10]

            if currSeq in record:
                record[currSeq] += 1
            else:
                record[currSeq] = 1

        return [seq for seq, count in record.items() if count > 1]
