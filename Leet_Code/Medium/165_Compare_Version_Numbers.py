""" 165. Compare Version Numbers

Question:

    Compare two versions and return 1 or -1 depending whether 1 > 2 or 2 < 1.

    The version numbers are in the form of 'x.y.z...'.

+++

Attempt:

    We iterate on the two given version numbers, and compare them char by char.
    Since it is ASCII encoded, should not worry about conversion - but if we do
    it this way, we should be weary of the zeros; hence, we will have to compute
    the entire word char value. This is only suggested if we are absolutely want
    to sure peak performance and int conversion is not allowed. Otherwise, int()
    converter will take care of all the edge cases.

+++

Result:

    Submission accepted at 20 ms.

"""

class Solution:

    def compareVersion(self, version1, version2):
        v1 = version1.split('.')
        v2 = version2.split('.')

        while len(v1) < len(v2):
            v1.append('0')
        while len(v2) < len(v1):
            v2.append('0')

        for curr1, curr2 in zip(v1, v2):
            if int(curr1) > int(curr2):
                return 1
            elif int(curr1) < int(curr2):
                return -1

        return 0
