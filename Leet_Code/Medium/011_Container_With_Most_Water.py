""" 11. Container With Most Water

Question:

    Given n non-negative integers where each represent a point at corrdinate, n
    vertical lines are drawn. Find two lines such that its area produces
    maximum.

+++

Attempt:

    In order to find the two lines that produces the maximum area, we can use
    the two pointers to start at each end. And since we are looking for max
    area, we can move the pointer that has the smaller height between two
    pointers

+++

Result:

    Submission accepted at 136 ms.

"""

class Solution:
    def findMaxArea(self, nums):
        if not nums: return 0
        maxArea = 0
        lo, hi = 0, len(nums)-1
        while lo < hi:
            currArea = min(nums[lo], nums[hi]) * (hi- lo)
            maxArea = max(currArea, maxArea)
            if nums[lo] < nums[hi]:
                lo += 1
            else:
                hi -= 1
        return maxArea
