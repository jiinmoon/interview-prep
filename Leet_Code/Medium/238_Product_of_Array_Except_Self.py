""" 238. Product of Array Except Self

Question:

    Given an array nums of n integers where n > 1, return an array output such
    that output[i] is equal to the product of all the elements of nums except
    nums[i].

+++

Attempt:

    If we are to approach this naively, we would simply perform product of all
    other elements for each element there is without that element itself. This
    naive approach has O(n^2) time complexity. The question asks us to solve
    this without using division and in O(n).

    [4, 5, 1, 8, 2]
    [1, 4, 20, 20, 160]
    [80, 16, 16, 2, 1]

    We make use of left/right product. Suppose we have following:

        nums        [1, 2, 3, 4]
        left        [1, 1, 2, 6]
        right       [24, 12, 4, 1]

    The final result would be product of two left and right arrays.

+++

Result:

    Submission accepted at 116 ms.
"""

class Solution:
    def productExceptSelf(self, nums):
        m = len(nums)
        result = [1] * m

        # first, populate result with left product array.
        for i in range(1, m):
            result[i] = nums[i-1] * result[i-1]

        # have a multiplying factor, whiis the right product array.
        # factor updates as we iterate along.
        factor = 1
        for i in range(m-1, -1, -1):
            result[i] *= factor
            factor *= nums[i]

        return result
