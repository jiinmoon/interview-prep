""" 114. Flatten Binary Tree to Linked list

Question:

    Given a binary tree, flatten it to a linked list in-place.

+++

Attempt:

    The flatten tree in the example looks like all the nodes are attached to the
    right side. The question does not ask us to make an ordered Linked List,
    hence, we traverse through the list, and transform the tree.

    Suppose a simple traversal method of visiting all the trees - we would be at
    first in leftmost node. Then, we would traverse further to find the right
    most node.

    Let's think iteratively: the general idea is that we want to move left nodes
    into the right nodes. First, we visit the node. Check right - if exist, we
    save its reference. Check left - if exist, we move over left to right;
    remember to set left to None. Now, we move to the right (which was
    previously left child if it was), and repeat the process. When we reach no
    more right to explore, then we can start looking at temp nodes that we have
    saved before - which are detached from anything else at the moment.

+++

Result:

    Submission accepted at 32 ms. How would I implement it recursively?

"""

class Solution:
    """ This solution is an iterative approach using stack. """

    def flattenTree(self, root):
        stack = []
        # process repeats until we have nodes to process.
        while root or stack:
            # when we are at node, first we check for the right child.
            # if exists, we need to save its reference.
            if root.right:
                stack.append(root)

            # we check the left child - if exists, we will attach to right.
            # previously we have saved the reference to root.right.
            if root.left:
                root.right = root.left
                root.left = None

            # if left child does not exist anymore, check stack to see whether
            # there are any detached right subtrees to explore.
            # however, this cannot happen after we moved left subtree to right.
            # there still might be potential leftsubtrees to explore.
            elif stack:
                root.right = stack.pop()
            root = root.right

