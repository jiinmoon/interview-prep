""" 220. Contains Duplicate III

Question:

    Given an array of integers, infd out whether there are two distinct indicies
    i and j such that the absolute difference between nums[i] and nums[j] is at
    most t and the absolute difference between i and j is at most k.

+++

Attempt:

    If we are unsure where to start, then let's examine few examples.

    [1, 2, 3, 1] , k=3, t=0

    i = 0, j = 3 would be the answer. abs(nums[i] - nums[j]) == 0 and abs(i-j)
    is also 3 which is at most k.

    Hence, we need a way to store the previous elements - when duplicate is
    encountered we update its value. The key would be the elements and values
    are the indicies.

+++

Result:

    The first approach unfortunately times out. It is understandable since it is
    a simple nested-loop algorithm. We can improve this further by using sliding
    window - that is, we do not have to look at every possible values, but only
    up to i + k + 1 - in this case, we wouldn't need a record as well.

    Just discovered that the test case has a lot of cases where t == 0...Hence,
    O(n^2) algorithms such as solution 1 or 2 can still be accepted once you
    screen out case where t == 0 as such:

        if t == 0 and there is no duplicate then return False.

    Submission accepted at 56 ms.


"""


class Solution:
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        """ logic is sound. but times out. """
        record = {}
        for i, num in enumerate(nums):
            record[num] = i
            for otherNum, j in record.items():
                if abs(num - otherNum) <= t and abs(i - j) <= k:
                    return True
        return False

class Solution2:
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        for i, num in enumerate(nums):
            for j in range(i+1, min(i+k+1, len(nums)):
                if abs(num - nums[j]) <= t:
                    return True
        return False



class Solution3:
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        for i in range(len(nums)-1):
            j = i + 1
            while j < len(nums) and (j- i) <= k:
                if abs(nums[i] - nums[j]) <= t:
                    return True
        return False
