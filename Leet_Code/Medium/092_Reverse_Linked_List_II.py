""" 92. Reverse Linked List II

Question:

    Reversea linked list from position m to n.

+++

Attempt:

    Take it slow - remember that we can create as many nodes to hold our
    positions. We iterate upto n - and as we are iterating, once we have past
    the m point, we start the reverse process.

+++

Result:

    Submission accepted at 32 ms. There are many ways to approach this problem -
    however, the solution posted at the site is very comprehensive and easy to
    understand. Travel up to m to find the start positions. Then, travel m - n
    more steps while reversing the nodes. Finish with adjusting the tail and
    new connection.

"""

class Solution:
    def reverseBetween(self, head, m, n):
        start, prev, curr = None, None, head
        i = 1
        while i != n:
            # save reference of next node.
            # this is due to the fact that we might reverse the nodes.
            temp = curr.next
            # have we reached the starting point?
            if i == m:
                # save the prev to start node for future reference.
                start = prev
            # past m - we start reverse process
            if i > m:
                curr.next = prev
            prev, curr = curr, temp

        if start:
            start.next.next = curr
            start.next = prev
        else:
            head.next = curr
            head = prev
        return head

