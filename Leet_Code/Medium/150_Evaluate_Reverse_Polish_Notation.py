""" 150. Evaluate Reverse Polish Notation

Question:

    Evaluate the value of an arithmetic expression in Reverse Polish Notation.

    Valid operators are +, -, *, /. Each operand may be an integer or another
    expression.

+++

Attempt:

    Reverse Polish Notation is best demonstrated by a simple example.

    '21+3*' would be equivalent to '2 + 1 * 3'.

    This can be simply implemented with a stack.

+++

Result:

    Submission accepted at 60 ms.

"""

class Solution:
    def eval(self, token, num1, num2):
        result = 0
        if token == '+':
            result = num1 + num2
        elif token == '-':
            result = num2 - num1
        elif token == '*':
            result = num1 * num2
        else:
            result = num2 // num1
        return result

    def evalRPN(self, tokens):
        in not tokens or len(tokens) == 0: return 0
        stack = []
        for token in tokens:
            if token in '+-*/':
                stack.append(self.eval(token, stack.pop(), stack.pop()))
            else:
                stack.append(int(token))
        return 0 if len(stack) == 0 else stack.pop()
