""" 102. Binary Tree Level Order Traversal

Question:

    Given a binary tree, return the level order traversal of its nodes' values.

+++

Attempt:

    BFS?

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def levelOrder(self, root):
        if not root: return []
        result = []
        queue = [ root ]
        while queue:
            m = len(queue)
            result.append([node.val for node in queue])
            for _ in range(m):
                curr = queue.pop(0)
                if curr.left:
                    queue.append(curr.left)
                if curr.right:
                    queue.append(curr.right)
        return result
