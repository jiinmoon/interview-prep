""" 207. Course Schedule

Question:

    There are a total of n courses you have to take, labeled from 0 to n-1.

    Some courses may have prerequisties, for example, to take course 0, you
    previously had to take course 1. This is expressed as [0, 1].

    Given the total number of courses and a list of prerequistie paris, is it
    possible for you to finish all courses?

+++

Attempt:

    We can think of this problem as a directional graph problem - and use the
    topological sorting which is BFS simply.

+++

Result:

    Submission accepted at 92 ms.

"""

from collections import defeaultdict

class Solution:
    def canFinish(self, numCourses, prereqs):
        # create neighbours list using hashmap struct.
        neighbours = defaultdict(list)
        # we need to find the starting nodes.
        # these are the nodes that which has no prereqs - indegree of 0.
        # also, doubles as a visited list.
        inEdges = [0] * numCourses
        # populate neighbours and inEdges.
        for (x, y) in prereqs:
            # y -> x
            record[y].append(x)
            inEdges[x] += 1
        # prepare BFS with queue.
        # starting nodes are found in inEdges.
        queue = [ i for i in range(numCourse) if inEdges[i] == 0 ]
        # start BFS.
        while queue:
            curr = queue.pop(0)
            for neighbour in neighbours[curr]:
                # mark visited.
                inEdges[neighbour] -= 1
                # after visiting, does it not have any more prereqs?
                # then, we can safely traverse on it for next.
                if inEdges[neighbour] == 0:
                    queue.append(neighbour)
        # after BFS is finished, if there are any in-degrees left over, no dice.
        return sum(inEdges) == 0

