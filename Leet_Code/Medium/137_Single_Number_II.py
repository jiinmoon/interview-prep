""" 137. Single Number II

Question:

    Given a non-empty array of integers, every element appears 'three' times
    except for one, which appears exactly once. Find that single one.

+++

Attempt:

    The algorithm it asks for should run in linear time complexity and without
    the extra memory. Of course, if memory was allowed, recording counting of
    each element would be the trivial answer to this question.

    We can think of this mathmatically - if we assume that all element in the
    array must appear three times, then we expect sum of all nums to equal to
    the all the elements we encounter in the array multiplied by 3. If not, then
    the difference must be the twice of the missing element.

    The problem is that does using set() structure violate the memory
    requirement? Or we work in-place, convert nums into set.

+++

Result:

    Submission accepted at 52 ms.

"""

class Solution:
    def singleNumber(self, nums):
        originalSum = sum(nums)
        nums = set(nums)
        return (3 * sum(nums)) - originalSum) // 2
