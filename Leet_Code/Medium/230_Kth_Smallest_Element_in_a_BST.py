""" 230. Kth Smallest Element in a BST

Question:

    Given a binary search tree, write a function kthSmallest to find the kth
    smallest element in it.

+++

Attempt:

    We traverse to the leftmost node. This would be the starting point - minimum
    of the whole tree where K = 1. From this point on, we can traverse k steps
    to find the next smallest elements in the BST.

+++

Result:

    Submission accepted at 48 ms.

"""

from collections import deque

class Solution:
    def kthSmallest(self, root, k):
        if not root: return
        curr = root
        stack = deque()
        while curr or stack:
            # traverse to leftmost node.
            while curr:
                stack.append(curr)
                curr = curr.left
            curr = stack.pop()
            if k == 1:
                return curr.val
            # still more to go. move onto right child and find again.
            curr = curr.right
            k -= 1

