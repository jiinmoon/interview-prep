""" 222. Count Complete Tree Nodes

Question:

    Given a complete binary tree, count the number of nodes.

+++

Attempt:

    Knowing the definition of the complete binary tree, we can find the number
    of nodes without having to traverse all the way - once we find the left-most
    node, we know which depth that the leaf nodes are located. Then, we can find
    the number of nodes by relationship between the height and nodes - which is
    2^(h + 1)-1, derived from 2^0 + 2^1 + 2^2 + ... + 2^h.

    But the last depth is the problem, how do we find the last leaf node? Here,
    we use the binary search algorithm. If this was a full complete binary tree,
    we know how many leaf nodes there should be - this will be the hi value of
    the binary search, where low value is the leftmost node.

    For every mid guess, we will have to traverse down to check whether that
    node exists.

+++

Result:

"""

from math import pow

class Solution:

    def _exist(self, root, mid):
        # the problem is that how do we know where mid is?
        # suppose that tree is numbered 1, 2, 3, 4...
        # then, we notice the pattern that left is even, right is odd.
        while mid > 1:
            if mid % 2 == 0:
                root = root.left
            else:
                root = root.right
            mid //= 2
        return root

    def countNodes(self, root):
        if not root: return 0
        node = root

        height = 0
        while node:
            node = node.left
            height += 1

        # as if the tree was full complete binary tree.
        # leftmost and rightmost node are numbered.
        leftmost = pow(2, height-1)
        rightmost = pow(2, height) -1

        while leftmost <= rightmost:
            mid = leftmost + (rightmost - leftmost) // 2

            # does mid exist?
            # we traverse down from root to check.
            if self._exist(root, mid):
                leftmost = mid + 1
            else:
                rightmost = mid - 1
        return leftmost
