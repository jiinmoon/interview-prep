""" 54. Sprial Matrix

Question:

    Given a matrix of m x n elements, return all elements of the matrix in
    sprial order (clockwise).

+++

Attempt:

    This is a good way to demonstrate the generator function - we create another
    function that generates the 2D coords given the dimensions of the matrix.

+++

Result:

    Submission accepted at 24 ms. Studying generator function later.

"""

class Solution:

    def spiralOrder(self, matrix):
        # many ways to generate coordinates, but we give start, and end points
        # for each rows and cols.
        def spiralCoords(r1, r2, c1, c2):
            # first, move across to right on first row.
            for col in range(c1, c2+1):
                yield r1, col
            # move down at right most col.
            # r1+1 since we have already visited the very first element before.
            for row in range(r1+1, r2+1):
                yield row, c2
            # at this point, we may have more row/cols to cover
            if r1 < r2 and c1 < c2:
                # move left at bottom most row.
                for col in range(c2-1, c1, -1):
                    yield r2, col
                # move up at leftmost col.
                for row in range(r2, r1, -1):
                    yield row, c1

        result = []
        r1, r2 = 0, len(matrix)-1
        c1, c2 = 0, len(matrix[0])-1
        while r1 <= r2 and c1 <= c2:
            for row, col in spiralOrder(r1, r2, c1, c2):
                result.append(matrix[row][col])
            r1 += 1; r2 -= 1; c1 += 1; c2 -= 1;
        return result

