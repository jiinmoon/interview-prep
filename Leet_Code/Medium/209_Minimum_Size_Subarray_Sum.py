""" 209. Minimum Size Subarray Sum

Question:

    Given an array of n positive integers and a positive integer s, find the
    minimal length of a contiguous subarray of which the sum >= s. If there
    isn't one, return 0 instead.

+++

Attempt:

    One important to note is that the array is populated only by positive
    integers. Hence, there are many ways to go about this. One, we consider
    contiguous subarray of all sizes to check for their sums. But this naive
    approach is slow and we can be a bit more smart about how we are approaching
    this problem. We maintain a total sum that we have seen thus far as we
    iterate along the array. Once we hit the point where our sum is above given
    target sum, then we know that what we have thus far is a candidate - compute
    the length of this array, and we now try to find the next candidate. We do
    this by advancing left pointer.

+++

Result:

    Submission accepted at 72 ms.

"""

class Solution:

    def findMinLen(self, nums, targetSum):
        if not nums or sum(nums) < targetSum: return 0
        currSum = l  = 0
        minLenThusFar = float('inf')
        for r in range(len(nums)):
            currSum += nums[r]
            # does adding current val made us exceed the limit?
            # if so, start advancing left pointer to update min len.
            while currSum >= s:
                minLenThusFar = min(minLenThusFar, r - l + 1)
                currSum -= nums[l]
                l += 1
        return minLenThusFar

