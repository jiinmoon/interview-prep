""" 93. Restore IP Addresses

Question:

    Given a string of possible IP Addresses, return all possible forms.

+++

Attempt:

    THe question at first appears to be a pattern matching problem. But we will
    have to do the dirty work all the way down. One way to approach this would
    be to use backtrack algorithm to try to build the IP addresses.

    It would be a process of checking how many steps we have taken
    (xxx.xxx.xxx.xxx == 4 in total), and whether the last addition is a valid
    form of IP.

    We do not have to worry about time complexity as it is a constant time
    check. Thus, we can devise another algorithm that actually takes
    consideration of all substrings, and try to build the ip that way.

+++

Result:

    Submission accepted at 28 ms. The second hack approach is one I like the
    most.

"""

class Solution:
    def isValid(self, s):
        m = len(s)
        return (m == 1 and int(s) <= 9) or /
                (m == 2 and int(s) <= 99) /
                (m == 3 and int(s) <= 255)

    def getCandidates(self, s, steps):
        # at each step, we can either have ip as long as 1, 2, or 3.
        result = []
        if len(s) < 4: return
        for i in range(3):
            if self.isValid(s[:i]):
                result.append((s[:i], i))
        return result

    def restoreIP(self, s):
        result = []

        def backtrack(s, path, steps):
            # base case
            if not steps:
                result.append('.'.join(path))
                return
            candidiates = self.getCandidates(s, steps)
            for candidate in candidates:
                backtrack(s[candidate[1], path + [candidate[0]]], steps-1)

        backtrack(s, [], 4)
        return result

class Solution:
    def isValid(self, *args):
        for ip in args:
            if len(ip) == 1 and int(ip) == 0:
                result.append(1)
            if ip[0] != '0' and int(ip) <= 255:
                result.append(1)
        return sum(result) == 4

    def restoreIP(self, s):
        result = []

        # three nested loops:
        # s[:i] + s[i+j] + s[j+k] + s[k:]
        for i in range(1, 4):
            for j in range(i+1, i+4):
                for k in range(j+1, j+4):
                    s1, s2, s3, s4 = s[:i], s[i:j], s[j:k], s[k:]
                    if len(s4) > 3 or len(s4) == 0:
                        continue
                    if self.isValid(s1, s2, s3, s4):
                        currIP = '.'.join([s1, s2, s3, s4])
                        result.append(currIP)
        return result

