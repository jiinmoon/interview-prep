""" 60. Permutation Sequence

Question:

    The set [1, 2, 3, ..., n] contains a total of n! unique permutations.

    Given n and k, find the k-th permutation sequence.

+++

Attempt:

    The naive approach would be to generate the permutation in order, and then
    return the k-th permutation sequence. Any backtracking can be used.

    Another method is using the mathmatical means. We can think of it as a
    key:value situation, getting the k-th permutation sequence is a matter of
    simply 'getting' it.

        123
        132
        213
        231
        312
        321

    For example, if we are to get the k = 2, how do we get the first digit?
    There are 3 options for n = 3, 1, 2, or 3. The first digit recurs 2 times in
    the permutation sequences. In terms of candidates, [1, 2, 3]. The selection
    expands to [1, 1, 2, 2, 3, 3]. Once 1 is chosen, we have [2, 3].

    The math is quite involved - I would be just happy to come up with
    backtracking answer.

+++

Result:

    Submission accepted at 28 ms. While I understand the concept, the math is
    still perplexing to me. I do not like this question.

"""
from math import factorial

class Solution:
    def getPermutation(self, n, k):
        candidates = [str(i) for i in range(1, n+1)]
        result = []
        while candidates:
            i = factorial(len(candidates) - 1)
            pos = (k-1) // i
            result.append(candidates[pos])
            candidates.pop(pos)
            k = k - (pos * i)
        return ''.join(result)

class Solution2:
    def getPermutation(self, n, k):
        result = []
        candidates = [str(i) for i in range(1, n+1)]
        def backtrack(candidates, path):
            if len(path) == n:
                result.append(list(path))
            if len(candidates) == 0:
                return
            for i in range(len(candidates)):
                path.append(candidates[i])
                backtrack(candidates[:i]+candidates[i+1:], path)
                path.pop()
        backtrack(candidates, path)
        return ''.join(result[-1])
