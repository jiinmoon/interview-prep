""" 74. Search a 2D Matrix

Question:

    Write an efficient algorithm that searches for a value in an m x n matrix.
    The integers are sorted from left to right and up to bottom.

+++

Attempt:

    This is a twist on the binary search algorithm. We first identify the row
    that the number is within bounds of, then perform binary search on that row.

    Or, we think of another way, since the array is sorted, we can move about
    the matrix. Starting from the matrix[0][-1], we can move row and col
    depending on whethre the lsat value of each row is greater or less than the
    target value. This is basically a binary search.

+++

Result:

    Submission accepted at 56 ms. However, the memory usage of 15 MB? I do not
    understand why it would take so much memory.

"""

class Solution:
    def searchMatrix(self, matrix, target):
        if not matrix: return False
        m, n = len(matrix), len(matrix[0])-1

        # starting from first row, last col.
        # if target is within the row, then should be less than the col value.
        # if target is not within the row, then value is greater than row value.
        row, col = 0, n
        while col >= 0 and row < m:
            if matrix[row][col] == target:
                return True
            elif matrix[row][col] > target:
                col -= 1
            else:
                row += 1

        return False

