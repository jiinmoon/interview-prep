""" 117. Populating Next Right Pointers in Each Node II

Question:

    Populate each next pointer to point to its next right node.

+++

Attempt:

    It is same as previous question, but now the binary tree is not perfect
    anymore - thus, we do not have gurantee that left and right exists. However,
    using BFS algorithm, it does not change anything.

+++

Result:

    Submission accepted at 44 ms.

"""

class Solution:
    def connect(self, root):
        if not root: return
        root.next = None
        queue = [ root ]
        while queue:
            temp = []
            while queue:
                curr = queue.pop(0)
                curr.next = queue[0] if queue else None
                if curr.left:
                    temp.append(curr.left)
                if curr.right:
                    temp.append(curr.right)
            queue = temp
        return root

