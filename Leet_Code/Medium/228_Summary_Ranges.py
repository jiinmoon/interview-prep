""" 228. Summary Ranges

Question:

    Given a sorted integer array without duplicates, return the summary of its
    ranges.

+++

Attempt:

    Two pointers will do it. One pointer to mark the beginning, then move the
    scan forward for count. Repeat until finished.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def summaryRanges(self, nums):
        result = []
        i = 0
        while i < len(nums):
            j = i + 1
            prev = nums[i]
            while j < len(nums) and prev + 1 == nums[j]:
                prev = nums[j]
                j += 1
            if nums[i] == prev:
                temp = str(prev)
            else:
                temp = '->'.join([str(nums[i]), str(prev)])
            result.append(temp)
            i = j
        return result
