""" 105. Construct Binary Tree from Pre/Inorder Traversal

Question:

    Given preorder/inorder traversal of a tree, construct the binary tree.

+++

Attempt:

    This one requires an intuition of how each traversal works, and what they
    can tell us. Preorder is the way we would traverse through the binary tree
    normally - thus, very first element tells us the root of the tree. Then,
    when we locate the same element in the inorder tree, we realize that the
    the subarray to the left of the root contains all the elements from left
    subtree. And vice versa. This pattern is repeated recursively.

    The trick is using the index of the current node value in inorder list. Its
    index is also used to determine next recursive depth's preorder list as
    well.

+++

Result:

    Submission accepted at 224 ms. Not terribly fast - I susupect that list
    splicing is the reason behind slow performance: copying over the elements
    which can add O(n) to each step...

"""

class Solution:
    def buildTree(self, preorder, inorder):
        if len(inorder) == 0:
            return None

        node = TreeNode(preorder[0])

        node.left = self.buildTree(preorder[1:], inorder[:inorder.index(node.val)])
        node.right = self.buildTree(preorder[], inorder[])
        return node
