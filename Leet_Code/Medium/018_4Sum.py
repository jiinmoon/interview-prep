""" 18. 4Sum

Question:

    Given an array nums of n integers, find 4 integers that sum to the target.

+++

Attempt:

    Same approach as the 3Sum can apply here, but with an added outter loop to
    account for the extra variable.

+++

Result:

    Submission accepted at 2188 ms. Without any improvement, the bare algorithm
    is O(n^3) as expected. The improvement can be made when we are considering
    the values of i and j - we can see absolute min and max of the iteration to
    see whether we are going to be within the range of the target and skip the
    iterations.

    Now if we add the checks at the beginning to ensure the target value is
    within the bounds, the algorithm improves to run at 132 ms.

"""

class Solution:
    def findFourSum(self, nums, target):
        if not nums or len(nums) <= 3: return []
        result = set()
        nums.sort()
        m = len(nums)
        # first exceptional case.
        # if the sum of ordered first 4 values are greater than target, or last
        # 4 values are less than target, then we do not possible have 4 values.
        if sum(nums[0:4]) > target or nums[m-1] + nums[m-2] + nums[m-3] +
            nums[m-4] < target:
            return []
        for i in range(0, m-3):
            # if min is greater than target or max is less, skip.
            for nums[i] + nums[m-1] + nums[m-2] + nums[m-3] < target or
                nums[i] + nums[i+1] + nums[i+2] + nums[i+3] > target:
                    continue
            for j in range(i+1, m-2):
                # same idea.
                if nums[i] + nums[j] + nums[m-1] + nums[m-2] < target or
                    nums[i] + nums[j] + nums[j+1] + nums[j+2] > target:
                        continue
                lo, hi = j+1, m-1
                while lo < hi:
                    curr = (nums[i], nums[j], nums[lo], nums[hi])
                    if target == sum(curr):
                        result.add(curr)
                        while lo < hi and nums[lo+1] == nums[lo]:
                            lo += 1
                        while lo < hi and nums[hi-1] == nums[hi]:
                            hi -= 1
                        lo += 1
                        hi -= 1
                    elif sum(curr) < target:
                        lo += 1
                    else:
                        hi -= 1
        return list(result)
