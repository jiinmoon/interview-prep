""" 129. Sum Root to Leaf Numbers

Question:

    Given a binary tree containing digits from 0-9, each root-to-leaf path could
    represent a number.

    Find the total sum of all root-to-leaf numbers.

+++

Attempt:

    This is a simple traversal to leaf question. We find all paths leading to
    leaf while saving them as numbers. Convert them to int and sum them up after
    traversal.

+++

Result:

    Submission accpetd at 32 ms.

"""

class Solution:
    def sumNumbers(self, root):
        result = []

        def backtrack(self, node, path):
            if not node: return 0
            if not node.left or not node.right:
                result.append(path + node.val)
            else:
                if root.left:
                    backtrack(node.left, (path + root.val) * 10)
                if root.right:
                    backtrack(node.right, (path + root.val) * 10)

        return sum(result)
