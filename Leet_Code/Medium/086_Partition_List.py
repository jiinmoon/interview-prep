""" 86. Partition List

Question:

    Given a linked list and a value x, partition it such that all nodes less
    than x come before nodes greater than or equal to x.

    You should preserve the original relative order of the nodes in each of the
    two partition.

+++

Attempt:

    We prepare another list where we are going to append the values greater than
    target value x. Then, at the end, we conjoin two list into one.

+++

Result:

    Submission accepted at 32 ms. It may not look as elegant and tricky, but it
    does its job well. It is very easy to see logic this way as well.

"""

class Solution:
    def partition(self, head, x):
        if not head or not head.next: return head
        dummyHead1 = curr1 = ListNode(float('-inf'))
        dummyHead2 = cuur2 = ListNode(float('-inf'))
        dummyHead1.next = curr1
        dummyHead2.next = curr2
        while head:
            if head.val < x:
                curr1.next = ListNode(head.val)
                curr1 = curr1.next
            else:
                curr2.next = ListNode(head.val)
                curr2 = curr2.next
            head = head.next
        curr2.next = None
        curr1.next = dummyHead2.next
        return dummyHead1.next
