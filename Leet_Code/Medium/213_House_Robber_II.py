""" 213. House Robber II

Questoin:

    Houses are arranged in circle - meaning that first house is the neighbour of
    the last one. And security alarm goes off when two consecutive houses are
    broken at same night.

    Given list of non-negative integers representing the amount of money of each
    house, determine the maximum amount of money you can rob.

+++

Attempt:

    Think in terms of general sense. At any given house, what is the maximum
    profit that we have made thus far? To answer this, we either not have robbed
    the previous house and rob this house; or roobed previous and skip this
    house. Either way, it depends on what choice we have made thus far.

    Hence, we maintain a DP array. At any point i, the dp[i] would be updated as
    max between previous value, dp[i-1] or current value + dp[i-2].

    Only complication is that we can start on two different points - do we start
    on 0 or 1? we simply have to perform both cases and find max between two.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def findMaxProfit(self, nums):
        if not nums or len(nums) == 0:
            return 0
        # think carefully. if only 3 elements, then you can rob only one of
        # them. remember that houses are in cycle.
        if len(nums) <= 3:
            return max(nums)

        def rob(houses):
            prevprev, prev = 0, 0
            for num in houses:
                prevprev, prev = max(prevprev, prev + num), prevprev
            return max(prev, prevprev)

        return max(
                rob(nums[:-1]),
                rob(nums[1:])
                )
