""" 17. Letter Combinations of a Phone Number

Question:

    Given a string containing digits from 2-9, return all possible letter
    combinations that the number could represent.

+++

Attempt:

    We prepare a phoneNumber mapping in hashmap, then iterate to find all the
    combinations possible.

+++

Result:

    Submission accepted at 28 ms. Need to think more flexibly about dfs in
    general and how I can utilize it.

"""

class Solution:
    def letterCombinations(self, digits):
        mapping = {
            '2' : 'abc', '3' : 'def', '4' : 'ghi',
            '5' : 'jkl', '6' : 'mno', '7' : 'pqrs',
            '8' : 'tuv', '9' : 'wxyz'
                }
        if not digits or digits == '': return []
        result = []

        def buildMap(s, curr):
            # base case: no more char to go through.
            if not len(s):
                result.append(curr)
                return
            # for every char mapped.
            for char in mapping(s[0]):
                # recursively call, building maps.
                buildMap(s[1:], curr + char)

        buildMap(digits, '')
        return result
