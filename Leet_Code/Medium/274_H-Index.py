""" 274. H-Index

Question:

    Given an array of citations (where each is non-negative integer) of a
    researcher, wrtie a function to compute the researcher's h-index.

    A scientist has index h if h of his/her N papers have at least h citations
    each, and other N-h papers have no more than h citations each.

+++

Attempt:

    We need some time to actually understand the problem first - let's look at
    an example.

        [3, 0, 6, 1, 5]

    Above citations mean that this researcher has 5 papers published in total
    and each reacced 3, 0, 6, 1, 5 citations respectively. Among them,
    researchers has 3 papers which are at least 3 citations each (3, 6, 5) and
    remaining two has no more than 3 (0, 1), this h-index is 3.

    First intuitive approach is that consider H-Index starting from 0 to find
    the maximum.

        [ 6, 5, 3, 1, 0 ]

    Other way would be to sort the array in reverse order. Then, we compare each
    value from behind to its current length of the array.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def hIndex(self, citations):
        citations.sort()
        citations = citations[-1]
        while citations and citations[-1] < len(citations):
            citations.pop()
        return len(citations)
