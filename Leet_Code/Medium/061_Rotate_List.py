""" 61. Rotate List

Question:

    Given a linked list, rotate the list to the right by k places, where k is
    non-negative.

+++

Attempt:

    The difficult is finding the point where the new head is going to be. This
    requires a length of a list - and take the modulo to given k. Then, the k-th
    node from the right would become the new head.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def length(self, head):
        result = 0
        while head:
            head = head.next
            result += 1
        return result

    def rotateRight(self, head, k):
        if not head or not head.next: return head

        listLength = self.length(head)
        steps = k % listLength
        newTail = newHead = head

        # move newHead forward.
        for _ in range(steps):
            newHead = newHead.next

        # newTail.next is new head and newHead is at tail.
        while newHead.next:
            newTail = newTail.next
            newHead = newHead.next

        # tail loops back to head.
        newHead.next = head
        # new head is newTail.next.
        newHead = newTail.next
        # make a new tail.
        newTail.next = None

        return newHead
