""" 62. Unique Paths

Question:

    A robot is located at the top-left corner of a m x n grid. How many unique
    ways can this robot reach the bottom-right corner of the grid?

+++

Attempt:

    This is a basic dp question: we generalize the question by asking this, at
    any given tile in the grid, how many different unique ways to reach that
    point? The answer is that we could have either came from left, or up. Thus,
    it is the sum of previous left tile and previous up tile values.

    Another method is to use backtracking algorithm.

+++

Result:

    Submission accepted at 28 ms. It is possible to use a single array for dp -
    instead of 2D.


"""
class Solution:
    def uniquePaths(self, m, n):
        dp = [[1 for _ in range(n)] for _ in range(m)]
        for i in range(1, m):
            for j in range(1, n):
                dp[i][j] = dp[i-1][j] + dp[i][j-1]
        return dp[-1][-1]

class Solution2:
    def uniquePaths(self, m, n):
        memo = {}
        def backtrack(row, col, m, n, memo)
            if row == m and col == n:
                return 1
            if row > m or col > n:
                return 0
            if tuple(row, col) not in memo:
                memo[tuple(row, col)] =
                    backtrack(row+1, col, m, n, memo) +
                    backtrack(row, col+1, m, n, memo)
            return memo[(tuple(row, col))]
        return backtrack(0, 0, m, n, memo)
