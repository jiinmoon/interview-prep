""" 98. Validate BST

Question:

    Given a binary tree, determine if it is a valid BST.

+++

Attempt:

    To check for whether the given tree fits the definition of the BST, we would
    have to traverse the tree and see if there exist any nodes that are out of
    bounds. But we have to be weary of the exceptional cases: we cannot just
    simply compare node.left and node.right against the parent's value. Due to
    recursive definition, ALL nodes have to observe minValue and maxValue
    determined as they move down the tree. For example, if we are traversing to
    the left, then the maxValue of the entire left-subtree is the current node's
    value. And vice versa.

+++

Result:

    Submission accepted at 32 ms. The only tricky part is the realizing the
    min/max update.

"""

class Solution:
    def isValidBST(self, root):
        def checkBST(node, minV, maxV):
            # base case: leaf is reached.
            if not node:
                return True
            # boundary check for current node.
            if minVal >= node.val or node.val >= maxVal:
                return False
            # recursively visit left and right whilst updating min/maxV.
            return checkBST(node.left, minV, node.val) and \
                checkBST(node.right, node.val, maxV)
        return checkBST(root, float('-inf'), float('inf'))


