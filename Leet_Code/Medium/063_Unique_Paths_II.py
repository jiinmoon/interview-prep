""" 63. Unique Paths II

Question:

    A same set-up but now, we have a obstacles are added to the grids.

+++

Attempt:

    The same basic principle applies here; that the dp solution still holds -
    for any arbitary tile in the grid, its value depends upon the two previous
    tiles from left and up. But now, we need an additional check to see whether
    current tile is the obstacle itself - which gets the value of 0.

    Suppose that dp[i][j] represents the total unique paths to tile at (i, j).

    dp[i][j] = dp[i-1][j] + dp[i][j-1] if obsGrid[i][j] is not obstacle else 0.



+++

Result:

    Submission accepted at 36 ms. A lot of little mistakes - not checking the i
    boundary in intial set-up and wrong boundary for j, which should have been
    [1, n).

"""

class Solution:
    def uniquePathsWithObstacles(self, obsGrid):
        m, n = len(obsGrid), len(obsGrid[0])
        dp = [[0 for _ in range(m)] for _ in range(n)]

        # set up first rows and cols.
        # 1 propagates as long as it is not obstacle'd.
        i = 0
        while i < n and obsGrid[0][i] == 0:
            dp[0][i] = 1
            i += 1

        i = 0
        while i < m and obsGrid[i][0] == 0:
            dp[i][0] = 1
            i += 1

        # perform dp.
        for i in range(m):
            for j in range(1, n):
                if obsGrid[i][j] != 1:
                    dp[i][j] = dp[i-1][j] + dp[i][j-1]

        return dp[-1][-1]

class Solution2:
    def uniquePathsWithObstacles(self, obsGrid):
        m, n = len(obsGrid)-1, len(obsGrid[0])-1
        memo = {}
        def bactrack(i, j, memo):
            # base case
            if i == 0 and j == 0:
                return 1 - obsGrid[0][0]
            if i < 0 or j < 0 or obsGrid[i][j]:
                return 0
            if tuple(i, j) not in memo:
                memo(tuple(i, j)) =  backtrack(i-1, j) + backtrack(i, j-1)
            return memo(tuple(i, j))

        return backtrack(m, n)
