""" 78. Subsets

Question:

    Find a set of all subsets of given list of numbers.

+++

Attempt:

    This is about finding the powerset of set. For a set of n elements, there
    should exists a 2**n subsets - which implies that the algorithm itself would
    be a costly one as well.

    There are many ways we can approach this problem. We could use a backtrack
    algorithm  - or simply iterateively add in the sets.

+++

Result:

    Submission accepted at 32 ms. The first solution gave an memory limit
    exceeded error. It was simply trying to do too much by copying the lists
    around. Instead of appending the subsets to the temp, we should have just
    appended to result list directly.

    Also, it is interesting to note the other solutions as well. Backtracking
    uses for-loop to call backtracks on each num. Bit-mask solution is also
    amazing but seems not as effective?

"""

class Solution:
    def powerSet(self, nums):
        """
        This solution gave an memory limit exceeded error. Notice that we should
        not have appeneded subset to temp! This was a mistake - of course we
        would have just iterate to infinity.
        """
        result = []
        for num in nums:
            temp = result.copy()
            # need to add current num into all existing sets in the result.
            for subset in temp:
                subset.append(num)
                temp.append(subset)
            # and we also need to append the element itself.
            temp.append(list(num))
            result = temp
        result.append(list())
        return result

class Solution2:
    def powerSet(self, nums):
        """
        This is the improved version.
        """
        result = [[]]
        for num in nums:
            temp = result.copy()
            for subset in temp:
                result.append(subset+[num])
        return result
