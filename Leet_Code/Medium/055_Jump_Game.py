""" 55. Jump Game

Question:

    Given an array of non-negative integers, you are initially positioned at the
    first index of the array.

    Each element in the array represents your max jump length at that position.
    Determine if you are able to reach the last index.

+++

Attempt:

    This question makes much more sense when we are thinking of it backward -
    instead of thinking of all the ways that we can reach the goal, we devise a
    way to see whether we can reach the starting position. This can be done by
    keep tracking the position variable - we update the last position that we
    are in as we iterate the array backwards. Suppose that at current position
    i, its jump value is nums[i]. Then, the maximum possible position that it
    can reach would be its sum. If their sum is greater or equal to last
    position that we were placed, then, we can move the last position to i since
    it is reachable.

    Obviously, this can apply forward as well. Just easier to think about it
    otherway around.

+++

Result:

    Submission accepted at 88 ms.

"""

class Solution:
    def canJump(self, nums):
        lastPos = len(nums)-1
        for i in range(len(nums)-1, -1, -1):
            if i + nums[i] >= lastPos:
                lastPos = i
        return lastPos == 0

    def canJump(self, nums):
        if not nums or len(nums) <= 1: return True
        maxJump = nums[0]
        for i in range(len(nums)):
            if nums[i] == 0 and maxJump <= i:
                return False
            maxJump = max(maxJump, nums[i] + i)
        return maxJump >= len(nums)
