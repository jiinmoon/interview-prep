""" 103. Binary Tree ZigZag Level Order Traversal

Question:

    Given a binary tree, return the zigzag level order traversal of its nodes'
    values.

+++

Attempt:

    Simply reverse on every other depth while traversing BFS?

+++

Result:

    Submission accepted at 32 ms. No need to think hard. Just a flag will do.

"""

class Solution:
    def zigzagLevelOrder(self, root):
        if not root: return []
        result = []
        queue = [ root ]
        flip = False
        while queue:
            m = len(queue)
            temp = [node.val for node in queue]
            if flip:
                temp = temp[::-1]
            flip = not flip
            result.append(temp)
            for _ in range(m):
                curr = queue.pop(0)
                if curr.left:
                    queue.append(curr.left)
                if curr.right:
                    queue.append(curr.right)
        return result
