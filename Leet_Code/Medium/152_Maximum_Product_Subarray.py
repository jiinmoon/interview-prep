""" 152. Maximum Product Subarray

Question:

    Given an integer array nums, find the contiguous subarray within an array
    containing at least one number which has the largest product.

+++

Attempt:

    While we may think of using the DP appraoch, the complication with the
    product operation is that negative nums can produce the greater product -
    thus, this also needs to be kept in check. Also, how about 0 as well? Which
    resets.

    This is done just as how we would have done with positive number: we would
    keep track of minimum value we found thus far as well as positive value.

+++

Result:

    Submission accepted at 88 ms.

"""

class Solution:
    def maxProduct(self, nums):
        if not nums: return
        result = maxThusFar = minThusFar = nums[0]
        for num in nums[1:]:
            temp = [num, maxThusFar * num, minThusFar * num]
            maxThusFar = max(temp)
            minThusFar = min(temp)
            result = max(result, maxThusFar)
        return result
