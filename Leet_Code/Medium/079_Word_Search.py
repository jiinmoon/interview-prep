""" 79. Word Search

Question:

    Given a 2D board and a word, find if the word exists in the grid.

    The word can be constructed from letters of sequentially adjacent cell (up,
    down, left or right). The same letter cell should not be used more than
    once.

+++

Attempt:

    This would be a backtracking problem. We iterate on the board, and whenever
    we find the start of the word, we perform backtracking - building the word
    by checking the neighbouring cells.

+++

Result:

    Submission accepted at 324 ms. This is faster than 83%, so it is not bad.
    Perhaps we could spped up by computing the next possible routes? For
    example, we look ahead at each depth, and if not, then we do not explore
    that depth as a candidate. With improvements made in checks, submission is
    now accepted at 260 ms.

"""

class Solution:
    def exist(self, board, word):
        if not board: return False
        m, n = len(board), len(board[0])

        def dfs(board, word, index, row, col):
            if index == len(word):
                return True

            if (row < 0 or row > m or \
                (col < 0 or col > n) \
                (board[row][col] != word[index]):
                return False

            temp = board[row][col]
            board[row][col] = '-1'
            result = dfs(board, word, index+1, row+1, col) or \
                    dfs(board, word, index+1, row, col+1) or \
                    dfs(board, word, index+1, row-1, col) or \
                    dfs(board, word, index+1, row, col -1)
            board[row][col] = temp
            return result

        for i in range(m):
            for j in range(n):
                if board[i][j] == word[0]:
                    if dfs(board, word, 0, row, col):
                        return True
        return False

class Solution2:
    def exist(self, board, word):
        if not board: return False
        m, n = len(board), len(board[0])

        def dfs(board, word, index, row, col):
            if index == len(word)-1:
                return True

            temp = board[row][col]
            board[row][col] = ' '
            nextMatch = word[index+1]
            candidates = []

            if row + 1 < m and board[row+1][col] == nextMatch:
                candidates.append((row+1, col))
            if row - 1 >= 0 and board[row-1][col] == nextMatch:
                candidates.append((row-1, col))
            if col + 1 < n and board[row][col+1] == nextMatch:
                candidates.append((row, col+1))
            if col - 1 < n and board[row][col-1] == nextMatch:
                candidates.append((row, col-1))

            for candidate in candidates:
                if dfs(board, word, index+1, candidate[0], candidate[1]):
                    return True

            board[row][col] = temp
            return False
