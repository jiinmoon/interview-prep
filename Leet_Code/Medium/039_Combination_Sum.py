""" 39. Combination Sum

Question:

    Given a set of candidate numbers, and a target number, find all unique
    combinations in candidates where the candidate numbers sums to target.

+++

Attempt:

    We use backtrack algorithm to build the candidates - these sum will also be
    tracked to see whether they are reached to 0.

+++

Result:

    Submission accepted at 48 ms. The base algorithm is dfs - should review this
    further.

"""

class Solution:
    def backtrack(self, result, candidates, path, pathSum, index):
        if pathSum < 0:
            return
        if pathSum == 0:
            result.append(list(path))
            return
        for i in range(index, len(candidates)):
            if candidates[i] < pathSum:
                path.append(candidates[i])
                self.backtrack(result, candidates, path, pathSum -
                        candidates[i], i)
                path.pop()

    def combinationSum(self, candidates, target):
        candidates.sort()
        result = []
        self.backtrack(result, candidates, [], target, 0)
        return result
