""" 116. Populating Next Right Pointers in Each Node

Question:

    You are given a perfect binary tree where all leaves are on the same level,
    and every parent has two children. Populate each next pointer to point to
    its next right node. If there is no next right node, set to NULL.

+++

Attempt:

    BFS.

+++

Result:

    Submission accepted at 56 ms. Simple enough...

"""

class Solution:
    def connect(self, root):
        if not root: return
        queue = [ root ]
        while queue:
            nextDepth = []
            while queue:
                curr = queue.pop(0)
                curr.next = queue[0] if queue else None
                if curr.left:
                    nextDepth.append(curr.left)
                    nextDepth.append(curr.right)
            queue = nextDepth
        return root

