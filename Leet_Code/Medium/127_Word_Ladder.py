""" 127. Word Ladder

Question:

    Given two words, and a dictionary of words, find the length of the shortest
    transformation sequence from begin to end such that only one letter can be
    changed at a time and each transformed word must exist in the word list.

+++

Attempt:

    We can view this problem as a graph problem essentially - one that is about
    finding the shortest path to the start to end node. Each node is the word
    and the edges are formed if two words are differ at most one char. Once
    graph is set-up, we can simply perform BFS to derived the shortest route to
    the end.

    Remember the subtle differences between BFS in trees and graphs - especially
    about cycles; hence, we need to have a record of visiting status on each
    nodes.

+++

Result:

    While the solution1 definitely works on small test cases, it takes too much
    time on precomputing the neighbours. We will have to change the tactics, and
    only compute neighbours that are completly necessary. Even after the
    modification to check only the neighbours when found, time still goes off
    the limit.

    Solution 2 is accepted at 576 ms. Learned that you have to do it hard way.

"""

from collections import defaultdict

class Solution:
    def isNeighbour(self, this, that):
        # is neighbour when this and that differ at one place.
        count = 0
        for i in range(len(this)):
            if this[i] != that[i]:
                count += 1
            if count > 1:
                return False
        return count == 1

    def wordLadder(self, start, end, words):
        if start == end: return 0
        # precompute all neighbours of given word.
        neighbours = defaultdict(list)
        for currWord in words:
            for otherWord in words:
                if currWord != otherWord and self.isNeighbour(currWord, otherWord):
                    neighbours[currWord].append(otherWord)
        queue = [ start ]
        visited = defaultdict(int)
        visited[start] = 1
        depth = 1
        while queue:
            m = len(queue)
            for _ in range(m):
                currWord, visited = queue.pop(0)
                if currWord == endWord:
                    return depth
                for neighbour in neighbours[currWord]:
                    if not visited[neighbour]:
                        queue.append(neighbour)
                        visited[neighbour] = 1
            depth += 1
        return 0


class Solution2:

    def ladderLength(self, beginWord, endWord, wordList):
        dictionary = set(wordList)
        queue = [ beginWord ]
        depth = 0
        while queue:
            depth += 1
            m = len(queue)
            for _ in range(m):
                currWord = queue.pop(0)
                for i in range(len(currWord)):
                    for c in range(ord('a'), ord('z')+1):
                        if ord(currWord[i]) == c:
                            continue
                        tempWord = currWord[:i] + chr(c) + currWord[i+1:]
                        if tempWord == endWord:
                            return depth
                        if tempWord in dictionary:
                            queue.append(tempWord)
                            dictionary.remove(tempWord)
        return 0
