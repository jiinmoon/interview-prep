""" 24. Swap Nodes in Pairs

Question:

    Given a linked list, swap every two adjacent nodes and return its head.

+++

Attempt:

    We can use as many pointers required to rewire the nodes! Save the pointers
    to the variables, then simply rework them.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def swapPairs(self, head):
        # we also need to work on head node as well. let's create dummy to hold.
        prev = dummyHead = ListNode(-1)
        dummyHead.next = head
        # we require at least two nodes for swapping.
        while prev.next and prev.next.next:
            node1, node2, node3 = prev.next, prev.next.next, prev.next.next.next
            prev.next = node2
            node2.next = node1
            node1.next = node3
            prev = node1
        return dummyHead.next
