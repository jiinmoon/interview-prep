""" 210. Course Schedule II

Question:

    There are a total of n courses you have to take (0 to n-1), which some may
    have prerequisites, denoted as a pair, [0, 1] meaning that course 0 requires
    1.

    Given total num courses and prerequisites, return ordering of courses
    required to take such that you can finish them all.

+++

Attempt:

    This is just an extension on the topological sorting algorithm - Perform BFS
    while checking for cycles.

+++

Result:

    Submission accepted at 100 ms.

"""

from collections import defaultdict

class Solution:
    def findOrder(self, numCourses, prereqs):
        result = []
        neighbours = defaultdict(list)
        inDegree = [0] * numCourses
        queue = []
        depth = 0

        # populate neighbours / inDegree.
        for course, prereq in prereqs:
            neighbours[prereq].append(course)
            inDegree[course] += 1

        # populate queue with starting nodes whose indegree == 0.
        for i in range(numCourses):
            if not inDegree[i]:
                queue.append(i)

        # start BFS.
        while queue:
            node = queue.pop(0)
            result.append(node)
            depth += 1
            for neighbour in neighbours[node]:
                inDegree[neighbour] -= 1
                if not inDegree[neighbour]:
                    queue.append(neighbour)
        if depth == numCourses:
            return result
        return []
