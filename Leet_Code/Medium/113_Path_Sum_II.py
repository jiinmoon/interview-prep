""" 113. Path Sum II

Question:

    Given a binary tree and a sum, find all root-to-leaf paths where each path's
    sum equals the given sum.

+++

Attempt:

    We carry pathSum down to the leaf - if pathSum we carried reaches 0 at
    leaf, then we add the current path to the list.

+++

Result:

    Submission accepted at 48 ms. Practice implementing it in iteratively as
    well.

"""

class Solution:
    def pathSum(self, root, target):
        if not root: return []
        result = []

        def dfs(node, path, pathSum):
            if not node: return
            if not node.left and not node.right and pathSum - node.val == 0:
                result.append(path + [node.val])
                return
            if node.left:
                dfs(node.left, pathSum - node.val, path + [node.val])
            if node.right:
                dfs(node.right, pathSum - node.val, path + [node.val])

        dfs(root, [], target)
        return result


