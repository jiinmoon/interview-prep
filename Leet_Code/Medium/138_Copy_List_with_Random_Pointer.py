""" 138. Copy List with Random Pointer

Question:

    A linked list is given such that each node contains an addtional random
    ppinter which could point to any node in the list or null.

    Return a deep copy of the list.

+++

Attempt:

    The problem lies with the fact that dealing with pointers that points to
    those that even not exist yet! For example, the very first head random
    pointer may point at something in the later.

    Thus, we keep a dictionary - a record for nodes that we create. For each
    node that we create, we will need to check for two things - whether its next
    node or its random node exist.

    If so, then they will also need to be created and stored in the record as
    well.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def copyRandomList(self, head):
        curr = head
        record = {}

        while curr:
            if curr not in record:
                record[curr] = Node(curr.val, None, None)
            if curr.next:
                if curr.next not in record:
                    record[curr.next] = Node(curr.next.val, None, None)
                record[curr].next = record[curr.next]
            if curr.random:
                if curr.random not in record:
                    record[curr.random] = Node(curr.random.val, None, None)
                record[curr].random = record[curr.random]
            curr = curr.next

        return record[head]
