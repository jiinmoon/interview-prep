""" 46. Permutations

Question:

    Given a collection of distinct integers, return all possible permutations.

+++

Attempt:

    One way to generate all the possible permutations for given list of integers
    would be to use a backtracking algorithm to explore all possibilities.

+++

Result:

    Submission accepted at 40 ms. Still a bit shaky on creating backtrack
    algorithm the way I wanted. Practice more on it.

"""

class Solution:

    def permute(self, nums):
        result = []

        # since we are not using start or index for indicator, we have to
        # decrease the selection in nums.
        def backtrack(nums, path, result):
            if not nums:
                result.append(list(path))
            for i in range(len(nums)):
                path.append(nums[i])
                backtrack(nums[:i] + nums[i+1:], path, result)
                path.pop()

        backtrack(nums, [], result)
        return result
