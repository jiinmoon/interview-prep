""" 106. Construct BT from Inorder and Postorder Traversal

Question:

    Given inorder and postorder traversal, construct BT.

+++

Attempt:

    Similar idea to the previous question: preorder list gave us a hint at what
    the root value is. Then, the intuition is that the postordering also gives
    us the root value - and use inorder to derive the contents of left and right
    subtree.

+++

Result:

    Submission accepted at 204 ms. Again, not the greatest of times due to list
    splicing. Few improvements would be prepare the inorder index map (reverse
    dict of val:index) and maybe avoid using lists and instead use pointer
    indexes.

"""

class Solution:
    def buildTree(self, inorder, postorder):
        # No more root to process.
        if len(postorder) == 0 or len(inorder) == 0:
            return None

        # Determine curr root - which is the last element in postorder.
        currNode = TreeNode(postorder.pop())
        inIndex = inorder.index(currNode.val)

        # recursively call left and right with their respective subarrays.
        # inorder is divided at inIndex.
        currNode.left = self.buildTree(inorder[:inIndex], postorder[:inIndex])
        currNode.right = self.buildTree(inorder[inIndex+1:], postorder[inIndex:])

        return currNode

