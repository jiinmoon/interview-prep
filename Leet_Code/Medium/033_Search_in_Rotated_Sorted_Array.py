""" 33. Search in Rotated Sorted Array

Question:

    Suppose an array is sorted in asecnding order, however it is rotated about
    unkonwn pivot. Find the target value.

+++

Attempt:

    Whether the array is rotated makes no difference as long as there exists a
    logical sort is applied such that we can have a directionality. We can still
    use binary search algorithm, but we first have to change it slightly such
    that we check which half of the array is in sorted order, and whether the
    target is within that bound.

+++

Result:

"""

class Solution:
    def search(self, nums, target):
        lo, hi = 0, len(nums)-1
        while lo <= hi:
            mid = lo + (hi - lo) // 2

            # fonud value right away?
            if nums[mid] == target:
                return mid

            # is bottom half in order?
            if nums[lo] <= nums[mid]:
                # is target within range?
                if nums[lo] <= target and nums[mid] > target:
                    hi = mid - 1
                else:
                    lo = mid + 1
            else:
                if nums[mid] < target and nums[hi] >= target:
                    lo = mid + 1
                else:
                    hi = mid - 1
        return -1
