""" 241. Different Ways to Add Parentheses

Question:

    Given a string of numbers and operators, return all possible results from
    computing all the different possible ways to group numbers and operators.
    The valid operators are +, - and *.

+++

Attempt:

    This is a interesting question. We group two operands and a single operator
    together to form another operand. This repeats recursively.

    Think of LISP or any list operations. Left and Right operands computed
    recursively.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def compute(self, input):
        result = []
        for i in range(len(input)):
            # operator?
            if input[i] in '+-*':
                # operator marks the division between two operands.
                # recursively find left and right.
                left = self.compute(input[:i])
                right = self.compute(input[i+1:])
                for op1 in left:
                    for op2 in right:
                        if input[i] == '+':
                            result.append(op1 + op2)
                        elif input[i] == '*':
                            result.append(op1 * op2)
                        else:
                            result.append(op1 - op2)
        if len(result) == 0:
            return [int(input)]
        return result
