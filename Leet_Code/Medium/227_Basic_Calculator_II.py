""" 227. Basic Calculator II

Question:

    Implement a basic calculator to evaluate a simple expression string.

    The string only contains non-negative integers and +, -, *, / operators and
    empty spaces.

+++

Attempt:

    The question cannot be simple read and evaluate approach due to the operator
    precedents (*, / before +, -). Here, we can use the stack. Into the stack,
    we add all the nums that we can find from the string. The default operation
    would be to add all the nums pushed unto the stack. However, two exceptions
    arises which are negative integers and * operations (and divide). Minus
    operation is simply an addition of negative integer. And in case of * or /,
    we pop the latest on the stack and perform the operation to put it back into
    the stack.

+++

Result:

    Submission accepted at 64 ms.

"""

class Solution:
    def calculate(self, s):
        if not s: return 0
        stack, op, num = [], '+', 0
        s += '+'
        for char in s:
            if char.isdigit():
                num = (num * 10) + int(char)
            elif char == ' ':
                pass
            else:
                if op == '+':
                    stack.append(num)
                elif op == '-':
                    stack.append(-num)
                elif sign == '*':
                    stack.append(stack.pop() * num)
                else:
                    stack.append(int(stack.pop() / num))
                op = char
                num = 0
        return sum(stack)
