""" 134. Gas Station

Question:

    There are N gas stations along a circular route, where the amonut of gas at
    each station i is gas[i]. If it costs cost[i] to travel from station i to
    its next station, return the starting gas station's index if you can travel
    around the circuit once in the clockwise direction.

+++

Attempt:

    We are given two list that shows us amount of gas you can obtain at i and
    cost to reach i+1. Obvious approach would be to try each starting point to
    see whether we can make the trip. Thus, simply take gast at i and take away
    cost[i] - at any point, if our gas tank goes minus, we cannot make the trip
    from that starting position.

    But we do not have to start at each position, the obvious starting positions
    would be those places where we can start with positive gas. This alone we
    can screen out a lot of possibilities.

    We can think about this problem even further. Just when do we have a
    gurantee that we can complete? The answer is when we have more gases than
    costs! Hence, once we find that the circuit can be completed, then the
    starting position would be the one that gives positive gas left. There must
    be a mathmatical proof for this but intuition is there.

+++

Result:

    Submission accepted at 48 ms. This one is really tricky one.

"""

class Solution:
    def canComplete(self, gases, costs):
        if sum(gases) < sum(costs): return -1
        tank, start = 0, 0
        for i, gas in enumerate(gases):
            tank += gas - cost[i]
            if tank < 0:
                tank, start = 0, i + 1
        return start


