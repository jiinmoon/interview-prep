""" 144. Binary Tree Preorder Traversal

Question:

    Given a binary tree, return the preorder traversal of its nodes' values.

+++

Attempt:

    Recursive solution is trivial - so we will focus on implementing the
    iterative solution. DFS tree traversal algorithm will be simple enough with
    its stack implementation. Learn diference between graph vs tree traversal.

+++

Result:

    Submission accepted at 20 ms.

"""

class Solution:
    def preorderTraversal(self, root):
        if not root: return []
        result = []
        stack = [ root ]
        while stack:
            curr = stack.pop()
            result.append(curr.val)
            if curr.right: stack.append(curr.right)
            if curr.left: stack.append(curr.left)
        return result
