""" 215. Kth Largest Element in an Array

Question:

    Find the kth largest element in an unsorted array. Note that it is the kth
    largest element in the sorted order, not the kth distinct element.

+++

Attempt:

    THe obivious approach would be sort the numbers and iterate to find the k-th
    largest. But if we can choose the right sorting algorithm, we can avoid
    having to sort the entire array, and find the element in less time. For this
    to work, we use QuickSort (quick select).

    Quick Sort works by finding the pivot and partitioning the array about the
    pivot. If we select mid element as our pivot, then we can avoid having to
    sort the entire array - only care for the pivot index and whether it is the
    given k. In short, compute the original k-th largest index. While sorting
    check to see whether pivot is at the correct place in the partitially sorted
    array, if it is we return it.

+++

Result:

    Submission accepted at 76 ms.

"""

from random import shuffle

class Solution:
    def _partition(self, nums, lo, hi, pivot):
        pivotVal = nums[pivot]
        nums[pivot], nums[hi] = nums[hi], nums[pivot]
        i, p = lo-1, hi
        for j in range(lo, hi):
            if nums[j] <= pivotVal:
                i += 1
                nums[i], nums[j] = nums[j], nums[i]
        nums[i+1], nums[p] = nums[p], nums[i+1]
        return i+1

    # quickSelect modified to find k-th largest.
    def _quickSelect(self, nums, lo, hi, step):
        # base case
        if lo >= hi: return nums[lo]
        pivot = lo + (hi - lo) // 2
        pivot = self._partition(nums, lo, hi, pivot)
        # is the pivot index at k-th from largest?
        if pivot == step:
            return nums[k]
        # otherwise, k index is either greater or lower.
        if step < pivot:
            return self._quickSelect(nums, lo, pivot-1, step)
        else:
            return self._quickSelect(nums, pivot+1, hi, step)



    def findKthLargest(self, nums, k):
        # randomize to avoid worst-case scenario.
        shuffle(nums)
        # len(nums)-k is the where k-th largest would be in sorted nums.
        return self._quickSelect(nums, 0, len(nums)-1, len(nums)-k)
