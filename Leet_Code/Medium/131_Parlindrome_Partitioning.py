""" 131. Palindrome Partitioning

Question:

    Given a string s, partition s such that every substring of the partition is
    a palindrome.

    Return all possible palindrome partitioning of s.

+++

Attempt:

    A simple DFS algorithm that considers each substring and checking for
    palindrome would suffice here.

+++

Result:

    Submission accepted at 72 ms.

"""

class Solution:
    def partition(self, s):
        result = []

        def backtrack(start, path):
            if start == len(s):
                result.append(list(path))
                return
            for i in range(start, len(s)):
                curr = s[start:i+1]
                if curr == curr[::-1]:
                    backtrack(i+1, path + [curr])

        backtrack(0, [])
        return result
