""" 240. Search a 2D Matrix II

Question:

    Write an efficient algorithm that searches for a value in an m x n matrix.
    This has following properties:

        - Integers in each row are sorted left to right (ascending).
        - Integers in each cols are sorted top to bottom (ascending).

+++

Attempt:

    Whenever sorted list is given, we can immediately think of binary search
    algorithm to try and locate the element.

    The naive approach would be to check each row for whether the value would be
    in that row, then perform the binary search algorithm. This would be O(n
    lg(n)).

    However, this matrix has a property that we need to carefully exaimne
    further. In this matrix, the minimum value is guranteed to be at mat[0][0],
    and the largest element would be at mat[n-1][m-1].

    This means that we alreay have a directionality as how we can proceed to
    move in this matrix. Suppose that we start at the end of first row. If the
    target value is greater, then we know that we must move to next row. If not,
    then the value should lie somewhere within the row. Again, when we are
    moving in the row, we encounter value that is smaller than target, then we
    should move down as it is in the increasing order.

    This can start in bottom-left as well.

+++

Result:

    Submission accepted at 40 ms.

"""

class Solution:
    def searchMatrix(self, matrix, target):
        m, n = len(matrix), len(matrix[0])
        row, col = 0, n-1
        while row < m and col >= 0:
            if target == matrix[row][col]:
                return True
            elif target > matrix[row][col]:
                row += 1
            else:
                col -= 1
        return False

