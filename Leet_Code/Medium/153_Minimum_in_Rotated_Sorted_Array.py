""" 153. Minimum in Rotated Sorted Array

Question:

    Suppose an array sorted in ascending order is rotated about unknown pivot -
    find the minimum element.

+++

Attempt:

    Fact that array being rotated does not change that we can still apply binary
    search algorithm. The rotated sorted array - when divided in half, will have
    at least a subarray that is in sorted order. Either mid < hi, implies that
    upper half is sorted, and all the upper values are greater than mid, so we
    search on bottom half. Vice versa.

+++

Result:

    Submission accepted at 40 ms.

"""

class Solution:
    def findMin(self, nums):
        if not nums: return 0
        lo, hi = 0, len(nums)-1
        while lo < hi:
            mid = lo + (hi - lo) // 2
            if nums[mid] < nums[hi]:
                hi = mid
            else:
                lo = mid + 1
        return nums[lo]
