""" 133. Clone Graph

Question:

    Given a reference of a node in a connected undirected graph, return a deep
    copy of the graph.

+++

Attempt:

    Just a simple traversal on the graph - whenever new node is discovered, we
    make a new Node. We use record to keep track of visited as well as
    neighbours.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def copyGraph(self, node):
        if not node: return

        q = [ node ]
        newRoot = Node(node.val, [])
        record = { node: newRoot }

        while q:
            currNode = queue.pop(0)
            for currNeighbour in currNode.neighbours:
                if currNeighbour not in record:
                    record[currNeighbour] = Node(currNeighbour.val, [])
                    queue.append(currNeighbour)
                record[currNode].neighbours.append(record[currNeighbour])

        return newRoot
