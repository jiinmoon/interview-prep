""" 143. Reorder List

Question:

    Given a singly linked list 0 > 1 > 2 > 3 ...., reorder it such that it is
    now 0 > n > 1 > n-1 > 2 > n-2 > ....

+++

Attempt:

    The question prohibits us from modifying the values of the list nodes thus,
    we will have to reattach some nodes in order. For this particular one, we
    split the list in half, and reverse the later half of the list. Then, we
    weave the two lists together into one.

+++

Result:

    Really weird that both solutions are about the same time complexity as well
    as space complexity for some reason. Why is it that second solution takes up
    just as much space as first one? First one is not using any temp list...

"""

class Solution:
    def splitList(self, head):
        prev = None
        slow = fast = head
        while fast:
            prev = slow
            slow = slow.next
            fast = fast.next.next if fast.next else fast.next
        if prev:
            prev.next = None
        return head, prev

    def reverseList(self, head):
        prev = None
        curr = head
        while curr:
            temp = curr.next
            curr.next = prev
            prev = curr
            curr = temp
        return pev

    def reorderList(self, head):
        if not head: return

        l1, l2 = self.splitList(head)
        l2 = self.reverseList(l2)

        # weave
        while l2:
            next1, next2 = l1.next, l2.next
            l1.next = l2
            l2.next = next1
            l1 = next1
            l2 = next2

class Solution2:
    def reorderList(self, head):
        stack = []
        curr = head
        while curr:
            stack.append(curr)
            curr = curr.next
        lo, hi = 0, len(stack) - 1
        while lo < hi:
            stack[lo].next = stack[hi]
            lo += 1
            stack[hi].next = stack[lo]
            hi -= 1
        if stack:
            stack[lo].next = None

