""" 236. Lowest Common Ancestor of a Binary Tree

Question:

    Given a binary tree, find the lowest common ancestor of two given nodes in
    the tree.

+++

Attempt:

    We do not have a link to parent - thus, we cannot simply traverse from given
    nodes p, and q to find the common ancestor. Also, we do not have any
    ordering within the tree as well; p and q can be anywhere and we do not know
    where it would be in the beginning.

    Then, only option left would be to traverse from root until we encounter p
    and q to see which node is the LCA.

+++

Result:

    Submission accepted at 60 ms.

"""

class Solution:
    def findLCA(self, root, p, q):
        if root in (None, p, q): return root
        left = self.findLCA(root.left, p, q)
        right = self.findLCA(root.right, p, q)
                root.right)
        return root if left and right else left or right
