""" 151. Reverse Words in a String

Question:

    Given an input string, reverse the string word by word.

+++

Attempt:

    huh...

+++

Result:

    Obviously...the first solution is probably not what author must have thought
    in mind.

"""

class Solution:
    def reverseWords(self, s):
        return ' '.join(s.split()[::-1])
