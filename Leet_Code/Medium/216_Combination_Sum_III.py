""" 216. Combination Sum III

Question:

    Find all possible combinations of k numbers that add up to a number n, given
    that only numbers from 1 to 9 can be used and each combination should be a
    unique set of numbers.

+++

Attempt:

    Like previous combination questions, we can use backtracking algorithm to
    try summing the numbers to generate path that may lead us to sum target. We
    have to be mindful of the non-duplicate combiatnions - thus, we need to take
    care not to select the duplicate elements on each step.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def combinationSum(self, k, n):
        result = []

        def generate(start, path):
            if len(path) > k or sum(path) > n:
                return
            if len(path) == k and sum(path) == 0:
                result.append(path[:])
                return
            for num in range(start, 10):
                generate(num+1, path + [num])

        generate(1, [])
        return result
