""" 139. Word Break

Question:

    Given a non-empty string s and a dictionary wordDict containing a list of
    non-empty words, determine if s can be segmented into a space-separated
    sequence of one or more dictionary words.

+++

Attempt:

    Perhaps backtracking algorithm would do it nicely. Convert wordDict into
    hashmap structure for ease of checks. Basically, perform a DFS on every
    possible substring with memo - where we will record whether curr string is
    in wordDict.

    Another possibility would be the dp approach: upto i, the s[:i] is either in
    the wordDict or not.

+++

Result:

    Submission accepted at 40 ms. Need to study more DP.

"""

class Solution:
    def wordBreak(self, s, wordDict):
        n = len(s)
        dp = [0] * (n+1)
        dp[0] = 1
        for i in range(1, n+1):
            for word in wordDict:
                if i >= len(word) and s[i-len(word):i] == word:
                    dp[i] |= dp[i-len(word)]
        return dp[n]
