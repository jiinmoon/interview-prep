""" 40. Combination Sum II

Question:

    ...

+++

Attempt:

    This is a similar question to the previous one - but here, we only care for
    unique cases. To screen out for unique elements, we simply pass the
    neighbours that which we have considered before.

+++

Result:

    Submission accepted at 64 ms.

"""

class Solution:
    def backtrack(self, result, candidates, path, pathSum, index):
        if pathSum < 0:
            return
        if pathSum == 0:
            result.append(list(path))
            return
        for i in range(index, len(candidates)):
            if i > index and candidates[i-1] == candidates[i]:
                continue
            if candidates[i] <= pathSum:
                path.append(candidates[i])
                self.backtrack(result, candidates, path, pathSum -
                        candidates[i], i+1)
                path.pop()

    def combinationSum2(self, candidates, target):
        result = []
        candidates.sort()
        self.backtrack(result, candidates, [], target, 0)
        return result
