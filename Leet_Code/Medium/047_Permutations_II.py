""" 47. Permutations II

Question:

    Given a collection of numbers that might contain duplicates, return all
    possible unique permutations.

+++

Attempt:

    Here, we use same backtrack algorithm as previous question - but we need to
    avoid selecting the duplicate elements. To do so, we first sort the array to
    group the duplicates together, then avoid picking the duplicates when
    choosing the candidates.

+++

Result:

    Submission accepted at 52 ms.

"""

class Solution:

    def permuteUnique(self, nums):
        result = []
        nums.sort()

        def backtrack(result, nums, path):
            if not nums or len(nums) == 0:
                result.append(list(path))
            else:
                for i in range(len(nums)):
                    if i != 0 and nums[i-1] == nums[i]:
                        continue
                    path.append(nums[i])
                    backtrack(result, nums[:i]+nums[i+1:], path)
                    path.pop()

        backtrack(result, nums, [])
        return result
