""" 142. Linked List Cycle II

Question:

    Given a linked list, return the node where the cycle begins. If there is no
    cycle, return null.

+++

Attempt:

    The question prevents us from making a changes to the given list - thus, a
    trick like reattaching the pointers back to head; thus, when we check for
    node.next.next, it will point back to head if node.next is the beginning of
    the cycle.

    Thus, we rely on one of the cycle detection algorithms - namely, Floyd's. It
    uses turtle and hare running around the list to see whether they come to a
    same spot. The particular thing behind this algorithm is that once they are
    at the same node, we can conclude that cycle exists. However, to find where
    the cycle begins, we can simply move the pointers again, after setting one
    of them back to the start of the list to see where they end up to be same.
    The math behind is not as intuitive, but when you do few practices, you can
    confirm that it indeed works and math checks out.

+++

Result:

    Submission accepted at 48 ms.

"""

class Solution:
    def detectCYcle(self, head):
        slow = fast = head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
            # Cycle Found.
            if slow == fast:
                slow = head
                while slow != fast:
                    slow = slow.next
                    fast = fast.next
                return slow
        return None
