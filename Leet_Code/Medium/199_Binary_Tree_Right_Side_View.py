""" 199. Binary Tree Right Side View

Question:

    Given a binary tree, imagine yourself standing on the right side of it,
    return the values of the nodes you can see ordered from top to bottom.

+++

Attempt:

    Understanding of how each traversal algorithms works is the key to solving
    this problem. In this case, we can easily use BFS. It traverses depth by
    depth - so, at each depth, we simply append the last val of the depth to our
    result list.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def rightSideView(self, root):
        if not root: return []
        result = []
        queue = [ root ]
        while queue:
            n = len(queue)
            result.append(queue[-1].val)
            for _ in range(n):
                curr = queue.pop(0)
                if curr.left:
                    queue.append(curr.left)
                if curr.right:
                    queue.append(curr.right)
        return result
