""" 19. Remove Nth Node From End of List

Question:

    Given a linked list, remove the n-th node from the end of the list and
    return its head.

+++

Attempt:

    Normally, you would have to traverse entire list to find the length of the
    list to find the node to remove. But we can have a pointer traverse n-th
    step forward, then until it reaches the end, we make it traverse. This would
    allow us reach the node in a single pass.

+++

Result:

    Submission accepted at 32 ms. The initial mistake in not thinking about
    removing the head node - had to use dummyHead!

"""

class Solution:
    def removeNthFromEnd(self, head, n):
        if not head: return
        runner = curr = head
        while n:
            runner = runner.next
            n -= 1
        dummyHead = curr = ListNode(-1)
        dummyHead.next = head
        while runner:
            runner = runner.next
            curr = curr.next
        curr.next = curr.next.next
        return dummyHead.next
