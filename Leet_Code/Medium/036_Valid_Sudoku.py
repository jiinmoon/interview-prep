""" 36. Valid Sudoku

Question:

    Determine whether given 9 x 9 sudoku board is valid.

+++

Attempt:

    We do not have to solve the sudoku to see whether the it is valid or not -
    but simply need to check the rule - that each rows, cols, or boxes cannot
    contain duplicates in numbers 1 to 9.

    Hence, we iterate on the 2D board. We save the encountered values into their
    respective rows and cols and boxes, and check to see whether they have
    violated the non-duplicate rule.

+++

Result:

    Submission accepted at 96 ms. Originally implemented using dict; but set is
    also acceptable. The box indicies are simply magic number.

"""

class Solution:
    def isValidSudoku(self, board):
        rows = [set() for _ in range(9)]
        cols = [set() for _ in range(9)]
        boxes = [set() for _ in range(9)]

        for row in range(9):
            for col in range(9):
                num = board[row][col]
                if num != '.':
                    # magical.
                    box = (row // 3) * 3 + (col // 3)
                    if num in rows[row] or num in cols[col] or num in
                    boxes[box]:
                        return False
                    rows[row].add(num)
                    cols[col].add(num)
                    boxes[box].add(num)
        return True
