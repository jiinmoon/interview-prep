""" 179. Largest Number

Question:

    Given a list of non negative integres, arrange them such that they form the
    largest number.

+++

Attempt:

    If the integers are guranteed to be a single digit, then it is simply sorted
    array reversed and converted back to the string. However, the complications
    arises since it is simply not.

    [10, 2] for example, if we follow above example will result in '102' while
    the accepted answer should be '210'. Thus, if we are still to use the
    sorting strategy, then we require different sorting key function
    (comparator) to work with.

    At core, all comparison based sorting algorithms are well, based on
    'comparing two values'. This is not a surprise. Then, how would we design
    such comparator function? Suppose we have a two values A and B. Then, we can
    determine by concatenating A+B and B+A; by comparison, we can then tell
    which one should come first.

+++

Result:

    Submission accepted at 36 ms.

"""

import functools

class Solution:

    def largestNumber(self, nums):
        nums = [str(num) for num in nums]
        sortedNums = list(
                sorted(
                    nums,
                    reverse=True,
                    key=functools.cmp_to_key(
                        lambda x, y: int(x+y) - int(y+x))))
        return '0' if sortedNums[0] == 0 else ''.join(sortedNums)
