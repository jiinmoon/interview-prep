""" 67. Add Binary

Question:

    Given two binary strings, return their sum (also a binary string).

+++

Attempt:

    The given input would be in binary and string format. Thus, we would need a
    way to convert them to integers, so that we can add two. And once we are
    done, the result would also have to be converted back into the binary string
    format as well. Thus, we would create two helper functions: binToInt and
    intToBin.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def binToInt(self, s):
        if not s or s == '': return 0
        i = result = 0
        for digit in s[::-1]:
            if digit == '1':
                result += 2 ** i
            i += 1
        return result

    def intToBin(self, num):
        if not num: return ''
        result = ''
        while num:
            result += str(num % 2)
            num //= 2
        return result

    def addBinary(self, a, b):
        if not a and not b: return ''
        if a: return a
        if b: return b
        result = self.binToInt(a) + self.binToInt(b)
        return self.intToBin(result)
