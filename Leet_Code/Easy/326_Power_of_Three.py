""" 326. Power of Three

Question:

    Determine whether given integer is a power of three.

+++

Attempt:

    There are multiple ways we could approach this problem. First, we know that
    there are only finite amount of power of threes that can fit inside the
    integer limit - hence, we create a hashmap of all power of threes and check
    against it at O(1).

    Another method would be simply looping and repeatedly divide the given
    integer by 3 to check the remainder.

    Other method is to check whether the number can divide the other known power
    of three evenly.

+++

Result:

    Submission accepted at 60 ms.

"""

class Solution:

    def isPowerOfThree(self, n):
        # 3 ** 19 = 1162261467
        # 2 ** 31 - 1 = 2147483648
        # 3 ** 20 overflows on the MAX_INTEGER.
        return n > 0 and (3 ** 19) % n == 0

