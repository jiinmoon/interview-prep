""" 203. Remove Linked List Elements

Question:

    Remove all elements form linked list of integers that have value, val.

+++

Attempt:

    We remove the vals as we iterate through. There are multiple ways we could
    do this such as instead of moving pointers, we could just overwrite the
    node.val itself. We would have to be careful of the cases where the head has
    the val that needs to be removed. To avoid this, we would use dummy head to
    guide us.

+++

Result:

    Submission accepted at 64 ms.

"""

class Solution:
    def removeElements(self, head, val):
        if not head: return
        dummyHead = curr = ListNode(-1)
        dummyHead.next = head
        # in beginning curr.next points at head.
        while curr.next:
            if curr.next.val == val:
                curr.next = curr.next.next
            else:
                curr = curr.next
        return dummyHead.next
