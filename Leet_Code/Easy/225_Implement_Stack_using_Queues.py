""" 225. Implement Stack using Queues

Question:

    Implement typical operations of stack using only queues.

+++

Attempt:

    The definition of stack is the LIFO operation. Thus, we have a problem in
    that to access the top of the stack, it would be placed at the end of the
    queue. Then, we have two options. Use two queues. We either need to
    sacrifice the time complexity on either push or pop to reorganize the queues
    in such way that we can access the top of the queue. This is achieved by
    shifting all elements from one queue to another.

    The better improvement would be to simply use a single queue where we will
    enqueue and dequeue at the same time with the queue.

+++

Result:

    Submission accepted at 20 ms.

"""

class MyStack:

    def __init__(self):
        self.q = []

    def push(self, x):
        self.q.append(x)
        # shift the elements in the queue such that last element is at front.
        for _ in range(len(self.q) - 1):
            self.q.append(self.q.pop(0))

    def pop(self):
        return self.q.pop(0)

    def top(self):
        return self.q[0]

    def empty(self):
        return len(self.q) == 0
