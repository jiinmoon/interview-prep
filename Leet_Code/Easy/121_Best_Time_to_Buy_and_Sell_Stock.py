""" 121. Best Time to Buy and Sell Stock

Question:

    Say you have an array for which the i-th element is the price of a given
    stock on day i.

    If you were only permitted to complete at most one transcation, design an
    algorithm to find the maximum profit.

+++

Attempt:

    Under this constraint of being only able to complete a single transcation,
    the question is finding the minimum price then finding the maximum prcie
    follows after the minimum price. However, the question is not as simple as
    finding the minimum in the array and then maximum as we can have cases such
    as minimum being the last value in the array in which case we will have to
    search next minimum...and so on.

    This complicates the algorithm too much. Thus, we simply go with the
    assumption that current price is minimum otherwise, we sell. This is done by
    maintaining two variables throughout the iteration, minThusFar and
    maxProfit. minThusFar keeps track of the minimum price that we have seen so
    far. And maxProfit is the one that given minThusFar, maxProfit made by
    considering current price.

+++

Result:

    Submission accepted at 56 ms.

"""

class Solution:
    def maxProfit(self, prices):
        minThusFar = float('inf')
        maxProfit = 0
        for price in prices:
            # for every price we encounter, we have two options.
            # either buy or sell.
            # we buy when we find out that this is the min price we have found
            # thus far. otherwise, we sell at current price, updating maxProfit.
            if price < minThusFar:
                minThusFar = price
            else:
                maxProfit = max(maxProfit, price - minThusFar)
        return maxProfit

