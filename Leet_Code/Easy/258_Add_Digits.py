""" 258. Add Digits

Question:

    Given a non-negative integer num, repeatedly add all its digits until the
    result has a single digit.

+++

Result:

    Submission accepted at 26 ms. There exists a O(1) solution that is based on
    mathmatical means (involving digital roots). Study more on it.

"""

class Solution:
    def addDigits(self, num):
        if num <= 0: return num
        while num > 9:
            temp = 0
            while num:
                temp += num % 10
                num //= 10
            num = temp
        return num
