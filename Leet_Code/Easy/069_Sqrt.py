""" 69. Sqrt(x)

Question:

    Implement 'int sqrt(int x)'.

    Compute and return the square root of x, where x is guranteed to be a
    non-negative integer. Truncate the decimal and return only int.

+++

Attempt:

    The naive approach would be to iterate all values 1 to x to check whether
    square of these value will result in x. But we can improve by realizing that
    this is a searching problem - that we are finding a value in a number line 1
    to x. Thus, we can implement binary search.

+++

Result:

    Submission acceptd at 36 ms. The tricky part is returning the lo - 1.

"""

class Solution:
    def sqrt(self, x):
        if x < 2: return x
        lo, hi = 1, x
        while lo < hi:
            mid = lo + (hi - lo) // 2
            if mid * mid == x:
                return mid
            elif mid * mid > x:
                hi = mid - 1
            else:
                lo = mid + 1
        return lo - 1

