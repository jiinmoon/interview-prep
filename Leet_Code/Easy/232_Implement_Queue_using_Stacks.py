""" 232. Implment Queue using Stacks

Question:

    Implement the following opereations of a queue using stacks.

+++

Attempt:

    Now, to implement a queue using stacks. If we push all unto a stack, the
    element that we want to dequeue would be at the bottom. Thus, we would
    require to stacks - one to push the elements in and others for poping the
    elements. The popStack in this case would be filled only when it is empty.

+++

Result:

    Submission accepted at 24 ms.

"""

class MyQueue:

    def __init__(self):
        self.pushS = []
        self.popS = []

    def enqueue(self, x):
        self.pushS.append(x)

    def dequeue(self):
        if len(self.popS) == 0:
            while self.pushS:
                self.popS.push(self.pushS.pop())
        return self.popS.pop()

    def top(self):
        if len(self.popS) == 0:
            while self.pushS:
                self.popS.push(self.pushS.pop())
        return self.popS[-1]

    def empty(self):
        return len(self.popS) == 0 and len(self.pushS) == 0
