""" 136. Single Number

Question:

    Given a non-empty array of integers, every element appears twice except for
    one. Find that single one.

+++

Attempt:

    The obvious solution is using a record to determine count of each element
    that we encountered. But the question also asks us whether we can implement
    it without using extra memory, in which case, we can use bit operation XOR.
    XOR has a property that cancels out the evenly occuring elements.

    Or, we can use the extra structure - but we simply delete the duplicate
    whenever we encountered them. Thus, we can keep a relatively low space.
    Hypothetically, it should be at worst case O(n) - having all duplicates
    scattered furthest away from one another.

+++

Result:

    Submission accepted at 88 ms.

"""

class Solution:
    def singleNumber(self, nums):
        if not nums or len(nums) == 0: return
        result = 0
        for num in nums:
            result ^= num
        return result
