""" 101. Symmetric Tree

Question:

    Given a binary tree, determine whether it is a mirror image of itself.

+++

Attempt:

    The trick is with this particular question is that realizing we can traverse
    on same tree - treating it as a separate tree. Thus, we create another
    function that accepts the two root of the tree. Then, we can traverse
    through the tree at two different ways at each iteration. This case, we can
    move in such manner to check the mirror image. For example, when we visit
    right child on one tree, go left child in another.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def isSymmetric(self, root):
        if not root: return True

        def checkSymmetry(p, q):
            if not p or not q: return p == q
            if p.val != q.val: return False
            return checkSymmetry(p.left, q.right) and checkSymmetry(p.right,
                    q.left)

        return checkSymmetry(root.left, root.right)
