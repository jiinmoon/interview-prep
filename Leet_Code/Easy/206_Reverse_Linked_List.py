""" 206. Reverse Linked List

Question:

    Reverse a singly linked list.

+++

Attempt:

    There are many ways to go about this, but one simple solution would be to
    prepare a new head of the linked list where the nodes from the given linked
    list will be constantly attached to - akin to inserting at 0 index.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def reverseList(self, head):
        # cannot reverse without at least two nodes
        if not head or not head.next: return head
        # curr to iterate over given linked list.
        # newHead is the new linked list entry point.
        curr = head
        newHead = None
        while curr:
            # save point to curr.next; otherwise, it will be garbase collected.
            temp = curr.next
            # insert at 0 index.
            curr.next = newHead
            # move newHead pointer back one; newly inserted node is the newHead.
            newHead = curr.next
            # move curr pointer forward; to the previously saved temp node.
            curr = temp
        return newHead
