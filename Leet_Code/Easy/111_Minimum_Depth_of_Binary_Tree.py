""" 111. Minimum Depth of Binary Tree

Question:

    Given a binary tree, find its minimum depth.

    The min depth is the number of nodes along the shortest path from the root
    node down to the nearest leaf node.

+++

Attempt:

    We can use various traversal algorithms - but in this case, BFS would do its
    job since BFS always finds the shortest route to the goal (in this case,
    closest leaf node).

+++

Result:

    Submission accepted at 44 ms.

"""

class Solution:
    def findMinDepth(self, root):
        if not root: return 0

        queue = [ root ]
        depth = 0

        while queue:
            m = len(queue)
            depth += 1
            for _ in range(m):
                curr = queue.pop(0)
                # leaf node reached.
                if not curr.left or not curr.right:
                    return depth
                # else continue.
                if curr.left:
                    queue.append(curr.left)
                if curr.right:
                    queue.append(curr.right)
