""" 107. Binary Tree Level Order Traversal II

Question:

    Given a binary tree, return the bottom-up level order traversal of its
    nodes' values.

+++

Attempt:

    This question can be approached in many different ways. However, we know
    there is a traversal algorithm that naturally visits the nodes in level
    order manner which is BFS. At start of each traversal at any depth, the
    BFS keeps track of its level in its queue.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def levelOrderBottomUp(self, root):
        if not root: return []
        result = []
        queue = [ root ]
        while queue:
            m = len(queue)
            nextLevel = []
            for _ in range(m):
                curr = queue.pop()
                nextLevel.append(curr.val)
                if curr.left:
                    queue.append(curr.left)
                if curr.right:
                    queue.append(curr.right)
            result.insert(0, nextLevel)
        return result
