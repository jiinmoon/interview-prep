""" 14. Longest Common Prefix

Question:

    Write a function to find the longest common prefix string amongst an array
    of strings.

+++

Attempt:

    Naive approach would result in highly unoptimal time complexity - to compare
    every strings to check for the prefix can take quite some time. The better
    approach would be to realize that once the array of string is alphabetically
    sorted, we only have to compare shortest and longest string to find the
    prefix.

+++

Result:

    Submission accepted at 24 ms. Other interesting method is to take notice
    that we can simply have first word in strs as a template - since it is
    prefix, it should present in all of them. If so, then only case we are
    concerned with is those that are shorter than chosen first word. We iterate
    on all other words to find the prefix, maintaining the furthestIndex.

"""

class Solution:
    def findPrefix(self, strs):
        if not strs: return ''
        strs.sort()
        s1, s2 = strs[0], strs[-1]
        i = 0
        while i < len(s1) and s1[i] == s2[i]:
            i += 1
        return s1[:i]

