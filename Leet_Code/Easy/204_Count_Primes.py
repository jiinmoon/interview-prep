""" 204. Count Primes

Question:

    Count the number of prime numbers less than a non-negative number, n.

+++

Attempt:

    Naive approach would be testing every integer upto n for primality. The
    better one would be to simply generate all the prime numbers upto n, which
    we have an efficient algorithm for which is Sieves of Eratosthenes.

    It works by considering every value upto sqrt(n). If the value is prime,
    then its multiples of itself would not be primes - thus, we mark them off.

+++

Result:

    Submission accepted at 208 ms.

"""

class Solution:
    def sievesOfEratosthenes(self, n):
        primes = [1] * n
        # base cases: 0 and 1 is not prime
        primes[0] = primes[1] = 0
        for i in range(2, int(n ** 0.5) + 1):
            if primes[i]:
                # if i is a prime, then every i-th value would be multiples of i
                # itself - which makes it not prime.
                # use list splicing for shortcut.
                primes[i*i:n:i] = [0] * len(primes(i*i:n:i))
        return primes

    def countPrimes(self, n):
        if n < 3: return 0
        return sum(self.sievesOfEratosthenes(n))
