""" 9. Palindrome Number

Question:

    Determine whether an integer is a palindrome. An integer is a palindrome
    when it reads the same as bacward as forward.

+++

Attempt:

    The step-by-step procedure would be to simply reverse given integer, then
    compare to see whether it is same. The edge cases would be negative numbers
    and those that are evenly divisible by 10 - implying ending with 0.

+++

Result:

    Submission accepted at 60 ms.

"""

class Solution:
    def reverseint(self, x):
        result = 0
        while x:
            result = (result * 10) + (x % 10)
            x //= 10
        return result

    def isPalindrome(self, x):
        if x == 0: return True
        if x < 0 or x % 10 == 0: return False
        reverseNum = self.reverseInt(x)
        return reverseNum == x

