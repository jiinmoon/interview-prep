""" 205. Isomorphic Strings

Question:

    Given wo strings s and t, determine if they are isomorphic.

+++

Attempt:

    One way to approach this problem would be to prepare two hashmap that maps
    that chars from each string unto each other. Thus, whenever we encounter
    same char again, we check whether they have been mapped to same char. If
    not, then we know the s and t are not isomorphic.

    The easiest way to think about this problem would be simply pair the chars
    from s and t. when we consider set of all chars in s and t and their
    pairings, all these length should be equal to one another. The Python golf
    type of answer would be then as follows:

        '''Python
        def isIsomorphic(self, s, t):
            return len(set(zip(s, t))) == len(set(s)) == len(set(t))
        '''

+++

Result:

    Submission accepted at 36 ms.


"""

class Solution:
    def isIsomorphic(self, s, t):
        if not s or not t: return True
        charMap = {}
        for i in range(len(t)):
            # if char from s already exist in the map, then it should have been
            # mapped to that particular char in t.
            if s[i] in charMap:
                if charMap[s[i]] != t[i]:
                    return False
            else:
                # if char is not in the map then we need to check whether char
                # from t has been mapped to some other char. if so, then it
                # indicates that char from s has been mapped to some other char
                # in t. return false
                if t[i] in charMap.values():
                    return False
                else:
                    # this is a new case. we encountered a truly new char
                    # mapping.
                    charMap[s[i]] = t[i]
        return True


