""" 53. Maximum Subarray

Question:

    Given a integer array nums, find the contiguous subarray which has the
    largest sum and return it.

+++

Attempt:

    This is a matter of considering currently encountered value. Suppose that we
    have a sum upto that point, then adding this value may create better maximum
    sum or it doesn't - at which point the contiguous sequence is broken. Thus,
    we maintain two variables throughout the iteration - currentSum and
    maxThusFar. currentSum is the local max, and maxThusFar would be the
    overall, considering max between currentSum and itself.

+++

Result:

    Submission accepted at 60 ms.

"""

class Solution:
    def maxSubarray(self, nums):
        # gurantee that array has at least a single value
        if len(nums) <= 1: return nums[0]
        maxThusFar = currentMax = nums[0]
        for num in nums[:1]:
            currentMax = max(currentMax + num, num)
            maxThusFar = max(currentMax, maxThusFar)
        return maxThusFar

