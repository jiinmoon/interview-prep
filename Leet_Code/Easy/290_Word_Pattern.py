""" 290. Word Pattern

Question:

    Given a pattern and a string str, find if str follows the same pattern.

+++

Attempt:

    We simply follow the logical step. We prepare a mapping between pattern to
    the words. When the pattern is found within the map, then it should have
    been mapped to the word previously encountered.

    Otherwise, the word is checked to see whether this has been mapped to some
    other pattern before - if so, then we know that it is mapped to some
    previous pattern that should have been correct. Return false.

    This question shares the exactly same logical steps as previous question.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def wordPattern(self, pattern, s):
        patternMap = {}
        s = s.strip().split(' ')
        if len(pattern) != len(s):
            return False
        for i, p in enumerate(pattern):
            if p in patternMap:
                if patternMap[p] != s[i]:
                    return False
            else:
                if s[i] in patternMap.values():
                    return False
                patternMap[p] = s[i]
        return True
