""" 202. Happy Number

Question:

    Write an algorithm to determine whether given integer is happy.

    It is happy when repeated sum of its digits squared ends in 1. Otherwise, it
    will loop in endless cycle.

+++

Attempt:

    At core, this is a cycle detection problem. To do so, we will use hashmap to
    store the previous results. And at any point, if we loop back and encounter
    values in the hashmap again, we know that the number is not happy.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def isHappy(self, n):
        if not n or n <= 0: return False
        prevResults = {}
        while 1:
            tempResult = 0
            while n:
                tempResult += (n % 10) ** 2
                n //= 10
            if tempResult == 1:
                return True
            if tempResult in prevResults:
                return False
            else:
                prevResults[tempResult] = 1
            n = tempResult

