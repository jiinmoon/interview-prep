""" 231. Power of Two

Question:

    Determine whether given integer is power of two.

+++

Attempt:

    There are several approaches to this. Given that there are only finite
    amount of power of twos present within bounds of integer (2**31 or 2** 63),
    we could simply make a record of all power of twos.

    Another method is to use the bit manipulation. We know that all the power of
    twos have to include at most a single bit. Thus, we either need to count all
    the bits in the given integer to see whether they sum to 1. Or better yet,
    we bit-mask given integer with integer value - 1. If the integer is power of
    two, we have a single bit on. Thus, taking one away would turn all other
    bits on except the most significant bit.

    i.e     4 = 0100
        4 - 1 = 0011

    Thus, AND bitoperation between two should result in 0.

+++

Result:

    Submission accepted at 24 ms. Mistake with not checking whether n is 0 to
    begin with.

"""

class Solution:
    def isPowerOfTwo(self, n):
        if n <= 0: return False
        return n&(n-1) == 0
