""" 108. Convert Sorted Array to Binary Search Tree

Question:

    Given an array where elements are sorted in ascending order, convert it to a
    height balanced BST.

+++

Attempt:

    The key here is making the BST that is height balanced. This is achieved by
    having equal number of nodes split into left and right. This means that
    simply at each step, we split the array into halves. Thus, say if we have
    access to the BST insert() function, we would start inserting the mid
    values, then split the array into lower and upper halves to repeat the
    process at each level.

+++

Result:

    Submission accepted at 76 ms.

"""

class Solution:
    def sortedArrayToBST(self, nums):
        if not nums or len(nums) == 0: return None

        def buildBST(nums):
            if not nums: return None
            mid = len(nums) // 2
            newNode = ListNode(nums[mid])
            # recursive call with lower half of array.
            newNode.left = buildBST(nums[:mid])
            # repeat on upper half of array.
            newNode.right = buildBST(nums[mid+1:])
            return newNode

        return buildBST(nums)
