""" 001. Two Sum

Question:

    Given an array of integers, return indices of the two numbers such that they
    add up to a specific target.

    Assume that each input have exactly one solution, and may not use the same
    element twice.

+++

Attempt:

    The naive approach would be to considering for every element against every
    other element, which results in unoptimal time complexity. To improve upon
    this, we can trade off space to speed up. This is achieved by saving
    previously seen elements and their positions in other data structure such as
    HashMap to quickly access it.

+++

Result:

    Submission accepted with runtime 48 ms.

"""

class Solution:
    def findTwoSum(self, nums, target):
        if not nums or len(nums) <= 1: return []
        record = {}
        for index, num in enumerate(nums):
            if num in record:
                return [record[num], index]
            else:
                record[target - num] = index
        return []

