""" 28. Implement strStr

Question:

    Implement strStr().

    Return the index of the first occurrence of needle in haystack, or -1 if
    needle is not part of haystack.

+++

Attempt:

    Finding a needle in the haystack is a very popular question and there are
    multiple algorithm exists out there (KMP, Rolling Hash...). But these
    algorithms are quite involved and honestly, they are too hard to come up
    with on the spot unless you just happen to memorize how they work.

    Thus, what would we do is simply traverse the haystack at every length of
    the needle to check whether this substring would be same as needle.

    This would not be as clever nor efficient as more notable pattern finding
    algorithms, but this would do in interview.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def strStr(self, haystack, needle):
        m, n = len(haystack), len(needle)
        if m < n: return -1
        for i in range(m - n + 1):
            if haystack[i:i+n] == needle:
                return i
        return -1

