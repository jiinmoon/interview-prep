""" 278. First Bad Version

Question:

    Given an API bool isBadVersion(version), implement a function to find the
    first bad version that which minimizes the call to API.

+++

Attempt:

    This in its essence is a search algorithm. Given n, we simply perform binary
    search - at each iteration, we take mid, and call API. But we do not stop -
    we need to continuously let the binary search take its place until the end
    to determine exactly the first version.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def findFirstBadVersion(self, n):
        lo, hi = 0, n
        while lo < hi:
            mid = lo + (hi - lo) // 2
            if isBadVersion(mid):
                # up to mid is bad.
                hi = mid
            else:
                lo = mid + 1
        return lo
