""" 226. Invert Binary Tree

Question:

    Invert a binary tree.

+++

Attempt:

    To invert a binary tree, we need to think recursively. At any given node, we
    need to exchange its child nodes. But we have to be careful not to exchange
    at the moment of traversal since that will ruin the algorithm. We first save
    the pointers returned, then once traversals are finished, we swap the node's
    left and right nodes returned.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def invertTree(self, root):
        if not root: return None
        leftChild = self.invertTree(root.left)
        rightChild = self.invertTree(root.right)
        root.left = rightChild
        root.right = leftChild
        return root
