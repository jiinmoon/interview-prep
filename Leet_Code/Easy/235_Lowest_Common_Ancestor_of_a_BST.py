""" 235. LCA of a BST

Question:

    Given a BST, find the lowest common ancestor of two given nodes.

+++

Attempt:

    The lowest common ancestor of two given nodes in the BST is the very first
    node that has a value lies in between the two nodes. Thus, we perform BFS on
    the BST to find that node.

+++

Result:

    Submission accepted at 80 ms.

"""

class Solution:
    def findLCA(self, root, p, q):
        if not root: return
        # treat p and q such that p <= q.
        if p.val > q.val:
            p, q = q, p
        queue = [ root ]
        while queue:
            curr = queue.pop()
            if curr.val >= p.val and curr.val <= q.val:
                return curr
            if curr.left:
                queue.append(curr.left)
            if curr.right:
                queue.append(curr.right)
