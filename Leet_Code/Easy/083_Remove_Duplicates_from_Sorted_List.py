""" 83. Remove Duplicates from Sorted List

Question:

    Given a sorted linked list, delete all duplicates such that each element
    appear only once.

+++

Attempt:

    The task is as simple as iterate on the list, and skip over duplicates if
    found. A single pointer is sufficient to check its val and its next.val. The
    fact that the list is sorted makes this easier as duplicates would be next
    to one another.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def removeDups(self, head):
        curr = head
        # we need at least two nodes to compare.
        while curr.next and curr.next.next:
            if curr.val == curr.next.val:
                curr.next = curr.next.next
            else:
                curr = curr.next
        return head
