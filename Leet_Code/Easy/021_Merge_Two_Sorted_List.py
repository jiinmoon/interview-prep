""" 21. Merge Two Sorted List

Question:

    Merge two sorted linked lists and return it as a new list. The new list
    should be splice of nodes from original lists - not a new list node with
    values copied over.

+++

Attempt:

    Simply, let us create a new list where we are going to append the nodes from
    the sorted lists. The obvious edge cases would be the different length
    between two given lists - in which case we append whichever is the remaining
    to the new list.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def mergeTwoSortedLists(self, l1, l2):
        # obvious checks.
        if not l1 and not l2: return None
        if not l1: return l2
        if not l2: return l1

        # create newHead to append nodes from l1 and l2.
        dummyHead = curr = ListNode(-1)
        dummyHead.next = curr

        # iterate on both l1 and l2 while they both have nodes.
        while l1 and l2:
            if l1.val > l2.val:
                temp = l1
                l1 = l1.next
            else:
                temp = l2
                l2 = l2.next
            curr.next = temp
            curr = curr.next

        # append remaining l1 or l2.
        if l1:
            curr.next = l1
        if l2:
            curr.next = l2

        return dummyHead.next
