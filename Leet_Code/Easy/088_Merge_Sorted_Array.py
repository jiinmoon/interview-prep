""" 88. Merge Sorted Array

Question:

    Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as
    one sorted array.

+++

Attempt:

    The nums1 will have enough capacity to hold all values over nums2, thus,
    this question will be mainly about maintaining two pointers - which keeps
    track of values from nums1 and nums2. We simply iterate on the nums1
    backwards, which would be the insertion point. Then, we compare two values
    indicated by two pointers from nums1 and nums2 to move over.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def merge(self, nums1, m, nums2, n):
        # two pointers for nums1 and nums2.
        first = m - 1
        second = n - 1
        # insertion pointer iterates backward on nums1.
        for i in range(m-1, -1, -1):
            # no more nums2 values means we are done.
            if second < 0: break
            # nums1 value goes in insertion point.
            if first >= 0 and nums[first] > nums2[second]:
                nums1[i] = nums[first]
                first -= 1
            else:
                nums1[i] = nums[second]
                second -= 1



