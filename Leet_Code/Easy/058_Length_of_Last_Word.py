""" 58. Length of Last Word

Question:

    Given a string s consists of upper/lowercase alphabets and emptyspace,
    return the length of the last word.

+++

Attempt:

    The question is rather trivial one if we fully utilize the string
    operations that are avilable to use. If we cannot, then it is simply a
    matter of traversing the string from behind to find the word. The
    exceptional case would be empty spaces in front and back of the string.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def lengthOfLastWord(self, s):
        s = s.strip()
        if not s or s == '': return 0
        return len(s.split()[-1])
