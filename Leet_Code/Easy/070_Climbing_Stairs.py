""" 70. Climbing Stairs

Question:

    You are climbing a stair case. It takes n steps to reach to the top.

    Each time you can either climb 1 or 2 steps. IN how many ways can you climb
    to the top.

+++

Attempt:

    If you lay down few examples, you will realize that this is a fib sequence,
    which makes sense. All different ways to reach nth step would be based on
    all the ways that previous and previous of previous iterations.

    If you are to implement recursive fib function, this would result in a
    unoptimal time complexity due to quickly increasing number of calls to the
    stack (every functions creates two another). Thus, we either want to
    implement using iterative means, or use memoization technique where we save
    the particular result to a record such that we do not have to call further
    if we have computed that particular value beforehand.

+++

Result:

    Submission accepted at 16 ms.

"""

class Solution:
    def climbStairs(self, n):
        def climb(n, record):
            # base cases of fib(0) and fib(1).
            if n < 0: return 0
            if n == 0: return 1
            # have we computed this value and stored it in record before?
            if n in record:
                return record[n]
            # new value, compute and store in record.
            record[n] = climb(n-1, record) + climb(n-2, record)
            return record[n]
        return climb(n, {})
