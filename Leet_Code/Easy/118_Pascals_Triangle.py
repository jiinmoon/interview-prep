""" 118. Pascal's Triangle

Question:

    Given a non-negative integer numRows, generate the first numRows of Pascal's
    Triangle.

+++

Attempt:

    There may be a mathmatical way to generate the given row in Pascal's
    Triangle - however, to approach this problem iteratively, we simply generate
    the triangles row after another - as each row is dependable on the result of
    previous row.

+++

Result:

    Submission accepted at 20 ms.


"""

class Solution:
    def generate(self, numRows):
        if numsRows <= 0: return []
        result = [[1]]
        for i in range(1, len(numRows)):
            prevRow = result[-1]
            newRow = [1]
            for j in range(1, len(prevRow)):
                newRow.append(prevRow[j-1] + prevRow[j])
            newRow.append(1)
            result.append(newRow)
        return result

