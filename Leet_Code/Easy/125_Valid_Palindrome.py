""" 125. Valid Palindrome

Question:

    Given a string, determine if it is a palindrome, considering only
    alphanumeric characters and ignoring cases.

+++

Attempt:

    The problem is easily approached by using two pointers which starts at both
    ends. The twist is that the string might be 'dirty', thus we either clean
    it up or consider moving pointers until we have a valid character to
    compare against.

+++

Result:

    Submission accepted at 48 ms.

"""

class Solution:
    def isValidPalindrome(self, s):
        lo, hi = 0, len(s)-1

        while lo < hi:
            # move pointers until valid.
            while lo < hi and not s[lo].isalnum():
                lo += 1
            while lo < hi and not s[hi].isalnum():
                hi -= 1
            # compare two chars, ignoring cases.
            if s[lo].lower() != s[hi].lower():
                return False
            lo += 1
            hi -= 1
        return True
