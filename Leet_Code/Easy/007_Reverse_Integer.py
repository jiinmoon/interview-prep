""" 7. Reverse Integer

Question:

    Given a 32-bit signed integer, reverse digits of an integer.

+++

Attempt:

    Simplest approach would be taking each digits out of the given integer, then
    repeatedly append to result variable. We have to take care of the sign of
    the integer as well as check whether reversed integer is within the bounds
    of max integer limit.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def reverseInt(self, num):
        if not num: return 0
        # cool trick; if-else is just as good.
        sign = (num > 0) - (num < 0)
        num = abs(num)
        result = 0
        # start reverse process.
        while num:
            result = (result * 10) + (num % 10)
            num //= 10
        return (sign * result) if result <= 2**31 - 1 else 0

