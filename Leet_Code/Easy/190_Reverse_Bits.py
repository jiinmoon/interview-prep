""" 190. Reverse Bits

Question:

    Reverse bits of a given 32 bits unsigned integer.

+++

Attempt:

    We iterate along the given integer bit by bit, reversing the bits.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def reverseBits(self, num):
        result = 0
        for _ in range(32):
            result = result | num & 1)
            result = result << 1
            num = num >> 1
        return result | n
