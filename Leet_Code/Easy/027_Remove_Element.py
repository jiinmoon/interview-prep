""" 27. Remove Element

Question:

    Given an array nums and a value val, remove all instances of that value
    in-place, and return new length.

    Do not allocate extra space and only modify in-place.

+++

Attempt:

    Similar question to previous question 26 - here, we can use the two pointer
    method again. ONly difference is that we are not checking whether insertion
    pointer and scanner pointer have same elements, but whether the scanner
    encountered the target value to be removed. If so, then we skip over copying
    over values to the insertion point.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def removeElement(self, nums, val):
        i = 0
        for j in range(len(nums)):
            if nums[j] != val:
                nums[i] = nums[j]
                i += 1
        return i

