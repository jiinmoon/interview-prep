""" 234. Palindrome Linked List

Question:

    Given a singly linked list, determine if it is a palindrome.

+++

Attempt:

    The simplest solution would be to prepare another record to save the values
    encountered while iterating on the linked list. Then check for palindrome.
    However, if only O(1) space is allowed, then we would reverse half of the
    linked list, then check for palindrome. This can be done by preparing two
    pointers - one that moves twice as fast. Thus, we can reach the half way
    point by the faster pointer reaches the end.

+++

Result:

    Submission accepted at 56 ms.

"""

class Solution:
    def reverseList(self, head):
        newHead = None
        curr = head
        while curr:
            temp = curr.next
            curr.next = newHead
            newHead = curr
            curr temp
        return newHead

    def isPalindrome(self, head):
        fast = slow = head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        slow = self.reverseList(slow)

        while head:
            if slow.val != head.val:
                return False
            slow = slow.next
            fast = fast.next
        return True
