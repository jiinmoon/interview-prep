""" 198. House Robber

Question:

    You are a professional rober planning to rob houses along a street. Each
    house has a certain amount of money stashed, the only constraint stopping
    you from robbing each is that adjacent houses have security system connected
    and it will contact police if two adjacent houses were robbed at same night.

    Given a list of non-negative integers representing money in each house, find
    maximum profit that can be made.

+++

Attempt:

    The problem is best approached by abstractly thinking about maxProfit made
    at any given moment. Suppose that we are on any of the houses. The maxProfit
    that can be made at this moment would be either the maxProfit that has been
    made 2 houses before plus the current house, or skip the current house, and
    keep the profit has been made upto previous house. Thus, this is a dynammic
    programming problem - we create a dp array to keep track of maximum profit
    made thus far.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def maxProfit(self, nums):
        if not nums or len(nums) == 0: return 0
        if len(nums) == 1: return nums[0]
        maxThusFar = [0 for _ in range(len(nums))]
        # this is a tricky part - how do we set up initial?
        # the base case is simple. but the next case is the maximum of either.
        maxThusFar[0], maxThusFar[1] = nums[0], max(nums[0], nums[1])
        for i in range(2, len(nums)):
            maxThusFar[i] = max(maxThusFar[i-2] + nums[i], maxThusFar[i-1])
        return maxThusFar[-1]
