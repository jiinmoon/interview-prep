""" 160. Intersection of Two Linked Lists

Question:

    Write a program to find the node at which the intersection of two singly
    linked lists begins.

+++

Attempt:

    To find where the two linked list overlap, we can basically treat this
    problem as a cycle problem. We prepare two runners at start of each list.
    Then, these runners are to move in the list. If they encounter end of the
    list, then they are placed back into the start of 'other' list. Thus,
    when they are equal to one another, this is the intersecting node.

+++

Result:

    Submission accepted at 160 ms. Notable other answer would be to create a
    hashmap to store the id of the nodes. Thus, we iterate on list1 while
    recording each nodes. And then when we iterate on list2, check against the
    hashmap, and return first node that matches.

"""

class Solution:
    def getIntersectingNode(self, head1, head2):
        if not head1 or not head2: return None

        runner1, runner2 = head1, head2
        while runner1 != runner2:
            runner1 = runner1.next if runner1 else head2
            runner2 = runner2.next if runner2 else head1
            if runner1 == runner2:
                return runner1
        return runner1
