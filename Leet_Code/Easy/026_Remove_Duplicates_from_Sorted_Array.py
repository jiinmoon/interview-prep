""" 26. Remove Duplicates from Sorted Array

Question:

    Given a sorted array nums, remove duplicates in-place such that each
    element may only appear once and return new length.

+++

Attempt:

    The fact that array is sorted gives us a hint that the duplicate elements
    would be next to one another. If so, then we can use two pointer method to
    approach this problem. We maintain a pointer for insertion, that which marks
    the index in the array where valid element would be placed and increments
    only when inserted. Another pointer would be scanning forward, finding the
    valid element to move over.

+++

Result:

    Submission accepted at 84 ms.

"""

class Solution:
    def removeDups(self, nums):
        if not nums: return 0
        if len(nums) <= 1: return len(nums)

        # so long as the element at the insertion pointer is not same as one
        # scanning forward, we move the elements over constantly.
        i = 0
        for j in range(1, len(nums)):
            if nums[i] != nums[j]:
                i += 1
                nums[i] = nums[j]
        return i + 1

