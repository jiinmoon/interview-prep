""" 283. Move Zeroes

Question:

    Given an array nums, write a function to move all 0s to the end of it while
    maintaining the relative order of the non-zero elements.

+++

Attempt:

    We simply maintain another pointer that act as a insertion point. And
    whenever we encounter non-zero element, we swap the places. This is repeated
    until end of the array.

+++

Result:

    Submission accepted at 52 ms.

"""

class Solution:
    def moveZeroes(self, nums):
        i = 0
        for j in range(len(nums)):
            if nums[j]:
                nums[i], nums[j] = nums[j], nums[i]
                i += 1

