""" 217. Contains Duplicate

Question:

    Given an array of integers, find if the array contains any duplicates.

+++

Attempt:

    The simplest method would be to prepare an extra data structure to record
    the encountered elements in the array. If we find the element that has been
    encountered before, we return true.

+++

Result:

    Submission accepted at 128 ms. Another notable Python golf method would be
    to simply convert the given nums into the set(), then compare the length
    between the set and the original array.

"""

class Solution:
    def containsDuplicate(self, nums):
        record = {}
        for num in nums:
            if num in record:
                return True
            else:
                record[num] = 1
        return False
