""" 141. Linked List Cycle

Question:

    Given a linked list, determine if it has a cycle in it.

+++

Attempt:

    There are many established cycle detection algorithms out there. But perhaps
    the most popular one would be the Floyd's Cycle Detection algorithm. The
    algorithm works as follows:

        1. Prepare two runners: slow and fast.
        2. fast moves through the list twice as faster than the slow.
        3. When fast catches up with the slow, we have a cycle. Otherwise, when
        fast reaches Null, we know that list does not have a cycle.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def hasCycle(self, head):
        slow = fast = head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
            if slow == fast:
                return True
        return False
