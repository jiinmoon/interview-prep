""" 122. Best Time to Buy and Sell Stock II

Question:

    Under same condition, but now you can complete as much transaction as you
    would like. Find the maximum profit.

+++

Attempt:

    At first glance, the question appears to be quite complex. How are we to
    determine maximum profit given that we can complete as many transaction as
    we would like? How do we even approach a problem such as this?

    The question becomes much simpler when we plot a graph of prices. The
    maximum profit that can be made is equal to all the positive differences
    between the prices.

+++

Result:

    Submission accepted at 56 ms.

"""

class Solution:
    def maxProfit(self, prices):
        maxProfit = 0
        for i in range(1, len(prices)):
            # as long as price differnce is positive, we add to proft.
            if prices[i-1] < prices[i]:
                maxProfit += prices[i] - prices[i-1]
        return maxProfit

