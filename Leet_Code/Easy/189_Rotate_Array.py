""" 189. Rotate Array

Question:

    Given an array, rotate the arrya to the right by k-steps where k >= 0.

+++

Attempt:

    This is a question about array operations, and how can we shift values down
    at the lower level. For Python, we cna use list operations that makes it
    much easier. Basically, we simply pop and insert repeatedly for k steps.

    Another approach would be reverse the list three times.

+++

Result:

    Submission accepted at 140 ms. This is surprisingly slow - perhaps, Python
    does not support the insertion and remove from list very well. It would have
    been better to approach with reversing option where we reverse the entire
    list, then divide the array into two halves at k index - at which we reverse
    both halves. The reversing method was accepted at 60 ms. But both cases, the
    memory usage was not fantastic...

"""

class Solution:
    def rotate(self, nums, k):
        for _ in range(k):
            nums.insert(0, nums.pop())

    def rotate2(self, nums, k):
        # changing subarray order is tricky with list splicing.
        def reverse(nums):
            return nums[::-1]
        k = k % len(nums)
        nums[:] = reverse(nums[:])
        nums[:k] = reverse(nums[:k])
        nums[k:] = reverse(nums[k:])
