""" 20. Valid Parentheses

Question:

    Given a string containing just the characters '([{}])', determine if the
    input string is valid. The string is valid if they are in same order and
    open brackets are closed by same kind.

+++

Attempt:

    We iterate on the given string with stack. We push the open brackets in
    stack, and whenever closed brackets are encountered, we check the top of
    stack to see it is the corresponding open bracket.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def isValidParen(self, s):
        bracketMap = {
                ')' : '(',
                '}' : '{',
                ']' : '['
                }
        stack []
        for bracket in s:
            if bracket in '([{':
                stack.append(bracket)
            else:
                if len(stack) == 0 or stack.pop() != backetMap[bracket]:
                    return False
        return len(stack) == 0
