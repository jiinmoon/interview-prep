""" 38. Count and Say

Question:

    1, 11, 21, 1211, 111221, ... is the conut-and-say sequence. Given an integer
    n where 1 <= n <= 30, generate the nth term of the count-and-say sequence.

+++

Attempt:

    Since we need n-1 th sequence to generate the n th sequence, we continuously
    generate the sequence upto n. This is a simple algorithm that reads the
    sequence as they are given 'count:num:count:num:...'.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def countAndSay(self, n):
        prev = '1'
        if n == 1: return prev
        while n > 1:
            i = 0
            curr = ''
            while i < len(prev):
                j = i + 1
                # find duplicate of prev[i]
                while j < len(prev) and prev[j] == prev[i]:
                    j += 1
                # update curr sequence with count:num = j-i:prev[i]
                curr += str(j-i) + str(prev[i])
                # fast forward i to find new sequence
                i = j
            prev = curr
            n -= 1
        return prev

