""" 169. Majority Element

Question:

    Given an array of size n, find the majority element. The majority element is
    the element that appears more than floor(n/2) times.

+++

Attempt:

    The simplest, and efficient way to approach this question would be just use
    the hashmap structure to record the values that we have seen thus far.
    Whenever the count of the values exceeds the majority, we return the value.

+++

Result:

    Submission accepted at 196 ms. It is interesting to note the other noble
    answers. First one is to sort the array - it would cost us O(n lg n), but
    would not require extra space given we sort inplace. Then, we can find the
    majority element at mid. Another method would be to randomize and guess the
    value. It has a good chance of picking the right answer in the first place -
    but for safe measure, for every guess we would double check our guess.

"""

class Solution:
    def majorityElement(self, nums);
    if len(nums) == 1: return nums[0]

    counter = {}
    for num in nums:
        if num in count:
            count[num] += 1
            if count[num] >= len(nums)//2:
                return num
        else:
            count[num] = 1
    return -1
