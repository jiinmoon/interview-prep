""" 263. Ugly Number

Question:

    Check whether the given positive integer is ugly - that has prime factors
    only 2, 3, and 5.

+++

Attempt:

    This requires a bit of mathmatical intuition - but the number whose prime
    factors include only 2, 3 and 5 should be completely divisible by these
    prime factors. Thus, while the given integer is evenly divisible, we
    repeatedly divide the number with prime factors. And it should only give us
    remainder of 1.

+++

Result:

    Submission accepted at 24 ms.

"""

class Solution:
    def isUgly(self, num):
        # only positive is concerned.
        if num <= 0: return False
        for prime in [2, 3, 5]:
            while num % prime == 0:
                num //= prime
        return num == 1
