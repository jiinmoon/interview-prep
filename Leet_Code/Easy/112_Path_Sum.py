""" 112. Path Sum

Question:

    Given a binary tree and a sum, determine if the tree has a root-to-leaf path
    such that adding up all the values along the path equals the given sum.

+++

Attempt:

    Here, we simply take the target value and continuously traverse down the
    tree while taking the node.val from it. Once we reach the leaf node, we can
    check whether the target value that we have been carrying along is equal to
    0, then we know that the path that sums to target value exists.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def hasPathSum(self, root, target):
        if not root: return False
        if not root.left and not root.right and root.val == target:
            return True
        return self.hasPathSum(root.left, target - root.val) or
            self.hasPathSum(root.right, target - root.val)
