""" 237. Delete Node in a Linked List

Question:

    Write a function to delete a node in a singlu linked list, given only access
    to that node.

+++

Attempt:

    One might wonder how to delete a node without access to that node's previous
    node to reattach the pointer to skip. The trick here is noticing that the
    question does not prevent us from changing the values of the nodes. Simply,
    we copy over node.next value, then delete the duplicate node to next.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def deleteNode(self, node):
        if not node: return
        node.val = node.next.val
        node.next = node.next.next
