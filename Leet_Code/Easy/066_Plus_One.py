""" 66. Plus One

Question:

    Given a non-empty array of digits representing a non-negative integer, plus
    one to the integer.

    The digits are stored such that the most significant digit is at the head of
    the list, and each element in the array contain a single digit.

+++

Attempt:

    The question does not restrict us to work in-place, hence, the easiest
    method would be then to convert array of digits into integer where we can
    perform arithmetic operation, then, restore back into the list format.

+++

Result:

    Submission accepted at 32 ms. The in-place answer would be better solution
    as we only need to check whether the values are 9 or not. If they flow over
    all the way, we then shift 1 over.

"""

class Solution:
    def listToInt(self, digits):
        result = 0
        for digit in digits:
            result = (result * 10) + digit
        return result

    def plusOne(self, digits):
        result = []
        num = self.listToInt(digits) + 1
        while num:
            result.insert(0, num % 10)
            num //= 10
        return result
