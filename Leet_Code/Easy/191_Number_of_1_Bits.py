""" 191. Number of 1 Bits

Question:

    Write a function that computes the hamming weight of the given integer.

+++

Attempt:

    Similar idea to previous question - we iterate along bit by bit, counting
    the number of 1s.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def hammingWeight(self, num):
        result = 0
        while num:
            result += num & 1
            num = num >> 1
        return result
