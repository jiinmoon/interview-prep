""" 119. Pascal's Triangle II

Question:

    Given a non-negative index k where k <= 33, return k-th index row of the
    Pascal's Triangle.

+++

Attempt:

    The algorithm is pretty much the same as previous. However, if we are to
    think of few improvements, since the index is gurantee'd to be less than 33,
    we could simply precompute all 33 rows and have it hardcoded. Or, store them
    in record and return whenever same value has been called again.

+++

Result:

    Submission accepted at 20 ms.

"""

class Solution:
    def getRow(self, rowIndex):
        result = [[1]]
        if rowIndex <= 0: return result[0]
        for i in range(rowIndex):
            prevRow = result[-1]
            newRow = [1]
            for j in range(1, len(prevRow)):
                newRow.append(prevRow[j-1] + prevRow[j])
            newRow.append(1)
            result.append(newRow)
        return result[-1]
