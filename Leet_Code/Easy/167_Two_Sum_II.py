""" 167. Two Sum II - input array is sorted

Question:

    Given an array of integers that is already sorted in ascending order, find
    two numbers such that they add up to a specific target number.

    The function twoSum should return indicices of the two numbers such that
    they add up to the target, where index1 < index2.

+++

Attempt:

    The fact that the array is now sorted gives us advantage - now, whenever we
    consider values to add to see whether it equals to target, we have a
    direction as to where to move our pointers. For example, if two values we
    are considering is less than the target, then we know that greater value
    must be considered hence moving the lower pointer towards the end.

+++

Result:

    Submission accepted at 52 ms.

"""

class Solution:
    def twoSum(self, nums, target):
        lo, hi = 0, len(nums)-1
        while lo < hi:
            currSum = nums[lo] + nums[hi]
            if currSum == target:
                # answer is not zero based.
                return [lo + 1, hi + 1]
            elif currSum < target:
                lo += 1
            else:
                hi -= 1
        return []

