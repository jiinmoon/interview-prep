""" 155. Min Stack

Question:

    Design a stack that supports its operations as well as 'min()' which returns
    the current min in the stack in O(1).

+++

Attempt:

    To support min() operation, we would simply have another stack prepared just
    to store minimum values inside. Thus, whenever we push, we push not only to
    our normal stack, but also push unto to minStack if the top of minStack is
    greater than the new value being pushed in. We pop from minStack whenever
    the value we pop from normal stack is equal to top of minStack.

+++

Result:

    Submission accepted at 52 ms.

"""

class MinStack:
    def __init__(self):
        self.s = []
        self.minS = []

    def push(self, val):
        self.s.append(val)
        if len(self.minS) != 0 and self.minS[-1] >= val:
            self.minS.append(val)

    def pop(self):
        temp = self.s.pop()
        if len(self.minS) != 0 and self.minS[-1] == temp:
            self.minS.pop()
        return temp

    def top(self):
        return self.s[-1]

    def getMin(self):
        return self.minS([-1])
