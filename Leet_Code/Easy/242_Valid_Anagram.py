""" 242. Valid Anagram

Question:

    Given two strings s and t, write a function to determine if t is an angaram
    of s.

+++

Attempt:

    Two strings would be anagram of each other if they are comprised of same
    number of characters. Thus, we count each char in one string, and then
    compare against another.

+++

Result:

    Submission accepted at 40 ms.

"""

from collections import defaultdict

class Solution:
    def isAnagram(self, s, t):
        if len(s) != len(t): return False
        charCount = defaultdict(int)
        for ch in s:
            charCount[ch] += 1
        for ch in t:
            charCount[ch] -= 1
            if charCount[ch] <= -1:
                return False
        return True
