""" 104. Maximum Depth of Binary Tree

Question:

    Given a binary tree, find its maximum depth.

+++

Attempt:

    Finding the height of the question is rather a trivial one. We simply
    traverse the tree anyway possible, and return its height.

+++

Result:

    Submission accepted at 36 ms.

"""

class Solution:
    def maxDepth(self, root):
        if not root: return 0
        return max(self.maxDepth(root.left), self.maxDepth(root.right)) + 1
