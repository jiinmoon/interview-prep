""" 219. Contains Duplicate II

Question:

    Given an array of integers, find two indicies where their num are same, but
    their indicies are at most k steps apart.

+++

Attempt:

    This is similar to the previous question. Again, we prepare a record. But in
    this case, we store the element's index as its value. Thus, whenever we
    encounter same element, we can check its index and take the difference to
    see whether they are less than K.

+++

Result:

    Submission accepted at 96 ms.

"""

class Solution:
    def containsDup(self, nums, k):
        record = {}
        for i, num in enumerate(nums):
            if num in nums and abs(record[num] - i) <= k:
                return True
            else:
                record[num] = i
        return False
