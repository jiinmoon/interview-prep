""" 344. Reverse String

Question:

    Write a function that reverses a string - which is given as a list of str.

+++

Attempt:

    Use two pointers at each end and swap them.

+++

Result:

    Submission accepted at 212 ms.

"""

class Solution:
    def reverseString(self, s):
        lo, hi = 0, len(s)-1
        while lo < hi:
            s[lo], s[hi] = s[hi], s[lo]
            lo += 1
            hi -= 1

