""" 100. Same Tree

Question:

    Given two binary trees, write a function to check if they are the same or
    not.

+++

Attempt:

    Use whichever traversal algorithm to check the nodes as we iterate along
    both of the trees at the same time.

+++

Result:

    Submission accepted at 28 ms.

"""

class Solution:
    def isSameTree(self, p, q):
        # base case: if one of them reached end, both should be at end.
        if not p or not q: return p == q
        # check values.
        if p.val != q.val: return False
        # recursive call to check childs
        return self.isSameTree(p.left, q.left) and self.isSameTree(p.right,
                q.right)

