""" 268. Missing Number

Question:

    Given an array containing n distinct numbers taken from 0, 1, ..., n, find
    the one that is missing from the array.

+++

Attempt:

    There are many approaches to this question. One would be to realize that all
    numbers should be present in this array. Thus, we use array index as a
    marker to check whether the given number is present. Another is to simply
    add all the numbers upto n then take the difference with the actual numbers
    present in the array.

+++

Result:

    Submission accepted at 140 ms. It appears that marking the indicies method
    is better in terms of time complexity - resulting in 56 ms.

"""

class Solution:
    def missingNumber(self, nums):
        return sum([i for i in range(len(nums)+1)]) - sum(nums)

    def missingNumber2(self, nums):
        if not nums or len(nums) == 0: return -1
        nums.append(0)
        for num in nums:
            while num != 'x':
                nums[num], num = 'x', nums[num]
        for i, num in enumerate(nums):
            if num != 'x':
                return i
        return -1
