""" 303. Range Sum Query - Immutable

Question:

    Given an integer array nums, find the sum of the elements between indicies i
    and j inclusive.

+++

Attempt:

    Of course, we could simply iterate along the the indicies given and return
    sum. But this is not efficient in time-wise, when the question lets us know
    that there would be many calls to sumRange function. Hence, this is a hint
    that we should prepare beforehand such that we can return the range sum as
    quickly as possible.

    One such way would be to simply prepare all the sums upto each indicies.

+++

Result:

    Submission accepted at 72 ms. Checking whether i is 0 is tricky part.

"""

class RangeSumArray:

    def __init__(self, nums):
        if not nums: return
        self.total = [0] * len(nums)
        self.total[0] = nums[0]
        for i in range(1, len(nums)):
            self.total[i] = self.total[i-1] + nums[i]

    def sumRange(self, i, j):
        if i == 0:
            return self.total[j]
        return self.total[j] - self.total[i-1]
