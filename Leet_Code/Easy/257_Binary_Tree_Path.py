""" 257. Binary Tree Path

Question:

    Given a binary tree, return all root-to-leaf paths.

+++

Attempt:

    Simple matter of traversing down to the leaf nodes, then returning the paths
    when we reach there.

+++

Result:

    Submission accepted at 32 ms.

"""

class Solution:
    def treePaths(self, root):
        result = []

        def findPath(node, path):
            # base case. leaf reached - append path to result.
            if not node: return
            if not node.left or not node.right:
                result.append('->'.join(map(str, path + [node.val])))
            else:
                # if not leaf, then recursively go down the tree
                if node.left:
                    findPath(node.left, path + [node.val])
                if node.right:
                    findPath(node.right, path + [node.val])

        findPath(root, [])
        return result
