""" 345. Reverse Vowels of a String

Question:

    Write a function that takes a string as input and reverse only the vowels of
    a string.

+++

Attempt:

    We can apply same approach as the reverse string problem; but in this case,
    we need to move the pointers to find the valid character (in this case,
    vowel).

+++

Result:

    Submission accepted at 44 ms.

"""

class Solution:
    def reverseVowels(self, s):
        vowels = 'aeiouAEIOU'
        lo, hi = 0, len(s)-1
        s = list(s)
        while lo < hi:
            while lo < hi and s[lo] not in vowels:
                lo += 1
            while lo < hi and s[hi] not in vowels:
                hi -= 1
            s[lo], s[hi] = s[hi], s[lo]
            lo += 1
            hi -= 1
        return ''.join(s)
