""" 110. Balanced Binary Tree

Question:

    Given a binary tree, determine if it is height-balanced.

+++

Attempt:

    The idea takes a form of twicking the height function. As we traverse
    through we check the heights of the left and right nodes. If they returned
    -1 (or error), we know that down in recursive call we have a imbalance in
    left and right subtrees. Otherwise, we also check for their current heights
    differences to check whether to return -1 or not.

    Otherwise, if all passes, we return height of current node as normally.

+++

Result:

    Submission accepted at 48 ms.

"""

class Solution:
    def isBalanced(self, root):
        if not root: return True

        def checkBalance(node):
            # base case.
            if not node: return 0
            # need to obtain heights of childs to see the diff.
            leftHeight = checkBalance(node.left)
            rightHeight = checkBalance(node.right)
            # check whether -1 has propagated or curr diff is the problem.
            if leftHeight == -1 or rightHeight == -1 or abs(leftHeight -
                    rightHeight) > 1:
                return -1
            return max(leftHeight, rightHeight) + 1

        return checkBalance(root) != -1
