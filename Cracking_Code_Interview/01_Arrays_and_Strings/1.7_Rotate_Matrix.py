""" 1.7 Rotate Matrix

Question:

    Given an image represented by an n x n matrix, where each pixel in the image
    is 4 bytes, write a method to rotate the image by 90 degrees. Can you do
    this in-place?

+++

Attempt:

    The simplest method which is not intuitive at first glance would be to
    reverse the matrix, and then exchange across the diagonal line. However,
    this is harder to guess, and our instinctive approach would be to move the
    elements to their rotated position - to do so, we need to generate the
    next coordinates.

"""

class Solution:
    def rotateMatrix(self, matrix):
        if not matrix: return matrix
        n = len(matrix)
        for layer in range(n//2):
            first = layer
            last = n - 1 - layer
            for i in range(first, last):
                offset = i - first
                temp = matrix[first][i]
                matrix[first][i] = matrix[last-offset][first]
                matrix[last-offset][first] = matrix[last][last-offset]
                matrix[last][last-offset] = matirx[i][last]
                matrix[i][last] = temp
        return




