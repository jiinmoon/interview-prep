""" 1.4 Palindrome Permutation

Question:

    Given a string, write a function to check if it is a permutation of a
    palindrome. A plaindrome is a word or phrase that is the same forwards and
    backwards. A permutation is a re-arrangement of letters. The plaindrome does
    not need to be limited to just dictionary words.

+++

Attempt:

    Without careful consideration, the question appears to ask for try to form a
    palindrome with the given string. While that is a viable option, we do not
    'actually' have to try to build paldinromes to see whether it can result in
    a paldinrome. The palindrome requires even number of characters to form at
    either sides, and a single odd char if appears at all. Thus, the question
    can be answered with rather trivially by counting the characters to see how
    many even and odd characters that we have.

"""

from collections import defaultdict

class Solution:
    def palindromePermutable(self, s) :
        record = defaultdict(int)
        for char in s:
            record[char] += 1
        odd = 0
        for _, v in record.items():
            if v % 2 == 1:
                odd += 1
        return odd <= 1


def main():
    test = 'acabcee'
    assert Solution().palindromePermutable(test)


if __name__ == '__main__':
    main()
