""" 1.2 Check Permutation

Question:

    Given two strings, write a method to decide if one is a permutation of the
    other.

+++

Attempt:

    The permutation simply means that two strings should be comprised of same
    number of characters. The straight forward approach would be to count each
    characters, then iterate on other string to see whether the count matches
    up. Another simple method would be to just sort the two given strings to see
    whether they are equal to one another.

"""

from collections import defaultdict

class Solution:
    def checkPermutation(self, s1, s2):
        # simple sorting method.
        s1 = ''.join(sorted(s1))
        s2 = ''.join(sorted(s2))
        return s1 == s2

class Solution2:
    def checkPermutation(self, s1, s2):
        # counting method.
        record = defaultdict(int)
        for char in s1:
            record[char] += 1
        for char in s2:
            if char not in s2 or record[char] == 0:
                return False
            record[char] -= 1
        return True

class main():
    s1 = 'abc'
    s2 = 'cba'

    assert Solution().checkPermutation(s1, s2)
    assert Solution2().checkPermutation(s1, s2)

if __name__ == '__main__':
    main()
