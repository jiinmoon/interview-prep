""" 1.6 String Compression

Question:

    Implement a method to perform basic string compression using the counts of
    repeated characters. For example, the string aabcccccaaa would become
    a2b1c5a3. If compressed string would not become smaller, return original
    string.

+++

Attempt:

    This question is similar to count-and-say sequence problem. This is simple
    enough - so long as given string is in sorted order, we simply count the
    chars as we encounter them.

    Apperently I misunderstood the question, we cannot sort. We just have to
    count char as we encounter them.

"""

from collections import Counter

class Solution:
    def compressString(self, s):
        # this is the misintrepreted work.
        result = []
        counter = Counter(s)
        for k, v in counter.items():
           result.append('{}{}'.format(k, v))
        print(result)
        return ''.join(result) if len(result) < len(s) else s

class Solution2:
    def compressString(self, s):
        result = []
        i = 0
        while i < len(s):
            j = i + 1
            count = 1
            while j < len(s) and s[i] == s[j]:
                j += 1
                count += 1
            result.append('{}{}'.format(s[i], count))
            i = j
        return ''.join(result) if len(result) < len(s) else s

def main():
    test = 'aabcccccaaa'
    assert Solution2().compressString(test) == 'a2b1c5a3'


if __name__ == '__main__':
    main()
