""" 1.8 Zero Matrix

Question:

    Write an algorithm such that if an element in an M x N matrix is 0, its
    entire row and column are set to 0.

+++

Attempt:

    We cannot blindly iterate and set entire row and col to zero as it would
    ruin future information about which row nad col to zero out. Thus, we need
    to store which row and cols to be zero'd. While we can prepare an extra
    structure for it, we can also simply use first row and col to mark.
    Obviously, we need a two boolean variables to denote whether first row or
    col itself needs to be zero'd when we are done.

"""

class Solution:
    def zeroCols(self, matrix, col):
        for row in range(len(matrix)):
            matrix[row][col] = 0

    def zeroRows(self, matrix, row):
        for col in range(len(matrix[0])):
            matrix[row][col] = 0

    def zeroMatrix(self, matrix):
        if not matrix: return matrix
        m, n = len(matrix), len(matrix[0])
        zeroFirstRow = 0 in matrix[0]
        zeroFirstCol = 0 in [x for x in matrix[i][0] for i in range(m)]

        for row in range(1, m):
            for col in range(1, n):
                if matrix[row][col] == 0:
                    matrix[row][0] = 0
                    matrix[0][col] = 0

        for col in range(1, n):
            if matrix[0][col] == 0:
                self.zeroCols(matrix, col)

        for row in range(1, m):
            if matrix[row][0] == 0:
                self.zeroRows(matrix, row)

        if zeroFirstRow:
            for col in range(1, n):
                matrix[0][col] = 0

        if zeroFirstCol:
            for row in range(1, m):
                matrix[row][0] = 0

        return matrix
