""" 1.3 URLify

Question:

    Write a method to replace all spaces in a string with '%20'. You may assume
    that the string has sufficient space at the end to hold the additional
    characters, and that you are given the true length of the string.

+++

Attempt:

    The question is rather trivial once one becomes very familar with string
    operations and what functions are made available to you. However, the
    purpose of the question is not that but whether can you manipulate the
    string char by char, so we can start that way.

    Since the list will be given to us with all the space necessary to contain
    the newly generated string, our strategy should be to work backwards - going
    forward requires too much shifting of the elements unless we want to
    temporary hold the string somewhere. The true length of the string is the
    beginning point, and insertion point would be the end of the list. Whenever
    we encounter char, simply copy over, otherwise, empty spaces will be filled
    by '%20'.

"""

class Solution:
    def URLify(self, s, n):
        insPtr = len(s)-1
        scanPtr = n - 1
        while scanPtr > 0:
            if s[scanPtr] == ' ':
                s[insPtr] = '0'
                s[insPtr-1] = '2'
                s[insPtr-2] = '%'
                insPtr -= 3
            else:
                s[insPtr] = s[scanPtr]
                insPtr -= 1
            scanPtr -= 1
        return ''.join(s)


class Solution2:
    # just demo.
    def URLify(self, s, n):
        return '%20'.join(s.split())


def main():
    test = list('Mr John Smith    ')
    print(test)
    assert Solution().URLify(test, 13) == 'Mr%20John%20Smith'

if __name__ == '__main__':
    main()
