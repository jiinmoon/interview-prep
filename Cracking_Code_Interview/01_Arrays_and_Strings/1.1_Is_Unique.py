""" 1.1 Is Unique

Question:

    Implement an algorithm to determine if a string has all unique characters.
    What if you cannot use additional data structures?

+++

Attempt:

    The obvious answer would be to use the extra data structure to record the
    elements encountered in the string - this will require both O(n) time and
    space.

    However, if we cannot use extra data structure, then we can do a brute force
    algorithm that compares each char against one another. This is O(n^2) time
    complexity. To improve upon this, we can modify and sort the strings to see
    whether duplicate neighbours exist.

"""

from collections import defaultdict

class Solution:
    def isUnique(self, s):
        # extra data structure allowed.
        record = defaultdict(int)
        for ch in s:
            record[ch] += 1
            if record[ch] > 1:
                return False
        return True

    def isUnique2(self, s):
        # no extra data structure.
        for i, c1 in enumerate(s):
            for c2 in s[i+1:]:
                if c1 == c2:
                    return False
        return True


def main():
    test1 = [
        'abc',
        'a',
        'abcdefghijklmnopqrstuvwxyz'
            ]
    test2 = [
        'aa',
        'aabc',
        'abbc',
        'abcc',
        'abcdefghijjklmnopqrstuvwxyz'
            ]
    for test in test1:
        assert Solution().isUnique(test)
        assert Solution().isUnique2(test)

    for test in test2:
        assert not Solution().isUnique(test)
        assert not Solution().isUnique2(test)

if __name__ == '__main__':
    main()

