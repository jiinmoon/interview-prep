""" 1.9 String Rotation

Question:

    Assume you have a method isSubstr that checks if one word is a substr of
    another. Given two strings, check if one is rotation of another using a
    single call to isSubstr.

+++

Attempt:

    The trick here lies in understanding that for given string x and y, if one
    is rotation of another, then when one string is concatenated to itself, the
    the rotated string must be within the concatenated string.

"""

class Solution:
    def stringRotation(self, s1, s2):
        return isSubstr(s1+s1, s2)
