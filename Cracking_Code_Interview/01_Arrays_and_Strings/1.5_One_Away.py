""" 1.5 One Away

Question:

    There are three types of edits: insert, remove, replace. Given two stringss,
    write a function to check if they are one edit away.

+++

Attempt:

    Firstable, we can quickly compare the length of the strings - if they differ
    by more than 1, then we cannot possibly fix it.

    To determine whether a character is single edit away - we simply try to edit
    it. We have choices, to start from shorter or longer one - or two strings
    are same length. If we start with shorter, we simply find a place where
    chars differ, and insert a char from longer one to compare. If two strings
    are of same length, then find where they differ and make correction to
    compare.

"""

class Solution:
    def deleteChar(self, shorter, longer):
        shorter = list(shorter)
        longer = list(longer)
        shorter.append(0)
        for i in range(len(shorter)):
            if shorter[i] != longer[i]:
                del longer[i]
                break
        shorter.pop()
        return ''.join(shorter), ''.join(longer)

    def replaceChar(self, shorter, longer):
        shorter = list(shorter)
        for i in range(len(shorter)):
            if shorter[i] != longer[i]:
                shorter[i] = longer[i]
                break
        return ''.join(shorter), longer

    def oneAway(self, s1, s2):
        m, n = len(s1), len(s2)
        if abs(m - n) > 1: return False
        shorter, longer = s1, s2
        if m > n:
            shorter, longer = longer, shorter
            s1, s2 = self.deleteChar(shorter, longer)
        elif m == n:
            s1, s2 = self.replaceChar(shorter, longer)
        print(s1, s2)
        return s1 == s2

def main():
    s1, s2 = 'abcdef', 'abcde'
    assert Solution().oneAway(s1, s2)
    s1, s2 = 'abc', 'axy'
    assert not Solution().oneAway(s1, s2)

if __name__ == '__main__':
    main()
